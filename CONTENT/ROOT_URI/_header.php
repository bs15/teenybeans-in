<!DOCTYPE html>
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Teeny Beans" />
<link rel="icon" type="image/png" sizes="16x16" href="/images/icon/favicon-16x16.png" async>
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Google Tag Manager -->
<script>
function loadDeferredIframe() {
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MFNWJVB');
        // this function will load the Google homepage into the iframe
        
    };
    window.onload = loadDeferredIframe();
</script>
<!-- End Google Tag Manager -->
