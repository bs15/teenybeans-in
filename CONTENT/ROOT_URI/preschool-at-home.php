<link rel="canonical" href="https://teenybeans.in/preschool-at-home" />
<title>Home preschool @Teeny Beans</title>
<meta name="description" content="The pandemic presents a unique customer acquisition
opportunity for Teeny Beans partners running their individual branded preschools.">
<style>
    	.slide.kenburns{
			background-image:url('/images/web/5.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/web/5-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!-- Banner Section -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns" style="background-image:url('/images/web/5.jpg');">
		<div class="bg-overlay"></div>
			<div class="container">
				<!-- <div class="slide-captions text-center text-light"> -->
				<div class="page-title text-center text-light">
					<h1>Home preschool @Teeny Beans</h1>
					<span>Start your own online classes with a individual branded Parent App with complete preschool digital curriculum in place</span>
				</div>
			</div>
	</div>
</section>

<!-- COVID-19 Reponse -->
<section  class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
			<picture class="team-image">      
					<source srcset="
					/images/sparsh/COVID-19-response.webp" type="image/webp">
					<source srcset="
					/images/sparsh/COVID-19-response.png" type="image/png">
				    <img src="/images/sparsh/COVID-19-response.webp" class="img-responsive" alt="COVID-19 Response from Teeny Beans" loading="lazy">
			</picture>
			</div>
			<div class="col-lg-8">
				<div class="heading-text heading-section">
					<h2>Digital Preschool with Teeny Beans</h2>
				</div>
				<p>Our strength has always been our flexible approach and customer orientation that borders on customer obsession. No matter what the circumstance, we fight to find the most appropriate solution for you. So when the world went ducking for cover in March end 2020, we went to the drawing board. By mid-April, our partner centers had started online classes and we had extended Sparsh for Parents to follow the daily curriculum and give shape to their effort to educate kids at home.</p>
				<p>We're not sure, but we may have been the first preschool service provider in India to actually respond in an organized way to the learning needs of children at home!</p>
			</div>
		</div>
	</div>
</section>


<!-- Top Points -->
<section>
    <div class="container">
        <div class="heading-text heading-section text-center">
            <h2>Online Classes from Teeny Beans</h2>
            <p>Our team is always at hand to guide you dedicatedly to successfully deliver your classes online. There are numerous things to keep in mind when delivering these classes - ideal batch strength, content for demonstration in-class, homework assistance, handling technology.</p>
        </div>
        <div class="row">
        	<div class="col-lg-4">
				<div class="icon-box effect medium clean">
					<div class="icon">
						<a href="#"><i class="far fa-lightbulb"></i></a>
					</div>
					<h3>Simple</h3>
					<p>You signing up for this does not automatically require you to upgrade your curriculum after you move into your regular in-class preschool program.</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="icon-box effect medium clean">
					<div class="icon">
						<a href="#"><i class="fab fa-accusoft"></i></a>
					</div>
					<h3>Effective</h3>
                    <p>Start your own online classes with an individual branded Parent App with a complete preschool digital curriculum in place</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="icon-box effect medium clean">
					<div class="icon">
						<a href="#"><i class="far fa-thumbs-up"></i></a>
					</div>
					<h3>Affordable</h3>
                    <p>All this at ZERO upgrade cost.Know more about how you should fight the COVID-19 pandemic to help your preschool franchise soar to newer heights.</p>
				</div>
			</div>
        </div>
    </div>
</section>

<!-- Call to Action -->
<section style="padding-top: 0; padding-bottom: 0;">
	<div style="background-image: url(images/web/general-bg.jpg);" class="call-to-action p-t-50 p-b-50 background-image mb-0">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-10">
	                <h3 class="text-light">Upgrade & Run A Digital Curriculum</h3>
	                <p class="text-light">
	                    Minimal technology charge and that too 100% deductible against preschool upgrade at partners discretion.
	                </p>
	            </div>
	            <div class="col-lg-2">
	                <a class="btn btn-light btn-outline" href="tel:+919748386431">Call us now!</a>
	            </div>
	        </div>
	    </div>
	</div>
</section>

<!-- Sparsh Intro -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				<div class="heading-text heading-section">
					<h2>Sparsh : Preschool Parent App</h2>
				</div>
				<p><strong><a href="https://play.google.com/store/apps/details?id=com.beanstalkedu.app.sparsh&utm_source=website&utm_campaign=tb_web&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">Sparsh</a></strong> is an app for parents and teachers to follow the <strong>Teeny Beans</strong> daily<br />curriculum. It contains</p>
				<p><strong>Content Book -</strong> A daily excerpt mapped to the day's content,</p>
				<p><strong>Workbook -</strong> has a printable workbook. parents can also sketch the worksheet if<br />there's no printer at home.</p>
				<p><strong>Videos -</strong> Relevant Videos supporting the daily curriculum to reinforce concepts</p>
				<p><strong>Teachers Demo -</strong> A demo video for explaining the concept</p>
				<p><strong>Lesson Plan -</strong> A very detailed lesson plan for the day is provided for both teacher<br />and the parent. Helps introduce concept and helps to reinforce it at home.</p>
				<p>Sparsh has really redefined the parent-teacher partnership like never before. Teachers and Parents are now completely in-sync and can wholeheartedly work together to support the kids learning process.</p>
			</div>
			<div class="col-lg-3">
			<picture class="team-image">      
					<source srcset="
					/images/sparsh/sparsh-banner1.webp" type="image/webp">
					<source srcset="
					/images/sparsh/sparsh-banner1.jpeg" type="image/jpeg">
				    <img src="/images/sparsh/sparsh-banner1.webp" class="img-responsive" alt="Sparsh : Preschool Parent App" loading="lazy">
			</picture>
			</div>
		</div>
	</div>
</section>

<!-- text Box with image -->
<div class="row col-no-margin equalize" data-equalize-item=".text-box">
    <div class="col-lg-3" style="background-image: url('/images/sparsh/sparsh-banner2.jpeg'); background-size: cover; background-repeat: no-repeat;">
        <!-- <div class="text-box">
            <a href="#">
                <i class="fa fa-paper-plane"></i>
                <h3>Tags & Models</h3>
                <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
            </a>
        </div> -->
    </div>

    <div class="col-lg-3" style="background-color: #86bc42;">
        <div class="text-box">
            <a href="#">
                <i class="fas fa-chart-pie"></i>
                <h3>Starting in 12 hours</h3>
                <p>Simply share email addresses of your parents and get started from the next day</p>
            </a>
        </div>
    </div>

    <div class="col-lg-3" style="background-image: url('/images/sparsh/sparsh-banner3.jpeg'); background-size: cover; background-repeat: no-repeat;">
        <!-- <div class="text-box">
            <a href="#">
                <i class="far fa-lightbulb"></i>
                <h3>Light & Dark</h3>
                <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
            </a>
        </div> -->
    </div>

    <div class="col-lg-3" style="background-color: #86bc42;">
        <div class="text-box">
            <a href="#">
                <i class="fa fa-language"></i>
                <h3>Continuity...</h3>
                <p>You signing up for this does not automatically require you to upgrade your curriculum after you move into your regular in-class preschool program.</p>
                <p>We are NOT a preschool franchisor. We are proud partners!</p>
            </a>
        </div>
    </div>
</div>

<!-- Contact Area -->
<section class="background-grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h3 class="text-uppercase">Upgrade for FREE</h3>
                <div class="row m-t-40">
                    <ul class="list-icon">
                        <li><i class="fa fa-check"></i>APP Access to Parents and Teachers.</li>
                        <li><i class="fa fa-check"></i>100% Access to our proprietary curriculum.</li>
                        <li><i class="fa fa-check"></i>Minimal technology charge and that too 100% deductible against preschool upgrade at partners discretion.</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="alert alert-danger" id="warning" style="display: none;">Fill all the fields!</div>
                <div class="alert alert-success" id="success" style="display: none;">Thank You! We will contact you soon!</div>
                <form  role="form" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" aria-required="true" name="widget-contact-form-name" required class="form-control required name" id="name1" placeholder="Enter your Name" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" id="email" aria-required="true" name="widget-contact-form-email" required class="form-control required email" placeholder="Enter your Email" />
                            <p id="emailError" style="color: red; display: none;">Enter a valid email address</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="telephone">Contact No</label>
                            <input class="form-control" id="phn" type="number" name="telephone" placeholder="Enter Contact number" required />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="addr">Address</label>
                            <input type="text" class="form-control" id="addr" name="address" placeholder="Current preschool location" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="addr">Add Name of your Preschool</label>
                        <input type="text" class="form-control" id="preschoolName" name="address" placeholder="Enter current preschool name" />
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea type="text" name="widget-contact-form-message" required rows="5" class="form-control required" id="message" placeholder="Enter your Message"></textarea>
                    </div>

                    <button class="btn" type="button" onclick="submitForm()"><i class="fa fa-paper-plane"></i>&nbsp; Send message</button>
                </form>
            </div>
            <div class="col-lg-4">
            	<div class="grid-layout testimonial testimonial-box" data- data-item="grid-item">
	                <div class="grid-item">
						<div class="testimonial-item">
							<img src="images/web/parent-at-preschool.jpg" alt="Parent at Beanstalk International Preschool" loading="lazy">
							<p>Arihana looks forward every day to his daily dose of learning! And we are happy with the realization that his learning has not stopped even though his school is closed.</p>
							<span>Sonu Singh</span>
							<span>Parent @ Beanstalk International Preschool</span>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var option= null;
    var name= null;
    var addr= null;
    var phn= null;
    var preschoolName = null;
    var email= null;
    var message= null;
    var warning = document.getElementById('warning');
    var success = document.getElementById('success');
    var emailError = document.getElementById('emailError');
    var req = document.getElementById("req");
    function submitForm(){
        name = document.getElementById("name1").value;
        phn = document.getElementById("phn").value;
        addr = document.getElementById("addr").value;
        email = document.getElementById("email").value;
        message = document.getElementById("message").value;
        success.style.display = "none";
        preschoolName = document.getElementById("preschoolName").value;;
        // console.log(name+phn+addr+email+message);
        if(name == '' || addr=='' || phn == '' || email == '' || message=='' || preschoolName == ''){
            warning.style.display = "block";
        }else{
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (reg.test(email) == false) 
            {
                emailError.style.display = "block";
            }else{
                emailError.style.display = "none";
                 warning.style.display = "none";
                let formData = new FormData();
                formData.append('formName', 'teenyBeansPreschoolAtHome');
                formData.append('Name', name);
                formData.append('Email', email);
                formData.append('Phone', phn);
                // formData.append('Address', addr);
                formData.append('MessageDetails', "Address= "+addr+". PreschoolName= "+preschoolName+". Message= "+message);
                fetch('https://api.teenybeans.in/API/contactFormProcessor/v1/', {
                  method: 'POST',
                  body: formData
                })
                .then(res => res.json())
                .then(json =>  {
                  // console.log(json);
                  // sendMail("enquiry@atheneumglobal.education");
                  // sendMail("enquiry@beanstalkedu.com");
                  sendMail("teenybeans.info@gmail.com");
                  // sendMail("chatterjeegouravking@gmail.com");
                  sendWelcomeMail(email);
                  success.style.display = "block";
                  document.getElementById("name1").value="";
                  document.getElementById("phn").value="";
                  document.getElementById("addr").value="";
                  document.getElementById("email").value="";
                  document.getElementById("message").value="";
                  document.getElementById("preschoolName").value="";

                  // window.location.href = "contact-back";
                  }
                );
            }
           
        }
        // if(name)
    }

     function sendMail(sendEmail) {
      // ---------Mail sent-----------------
      
      let formData = new FormData();
      formData.append("sendMail", "true");
      formData.append("reciever", sendEmail);
      formData.append("sender", "Teenybeans");
      formData.append("senderMail", "no-reply@teenybeans.in");
      formData.append("subject", "New HomePreschool Form Fillup");
      formData.append(
        "message",
        "<html><body><p>New HomePreschool form is filled up.</p><br><p>User Details:-</p><table><tr><th>Name:- </th><td>:- " +
          name +
          "</td></tr><tr><th>Email:- </th><td>:- " +
          email +
          "</td></tr><tr><th>Phone:- </th><td>:- " +
          phn +
          "</td></tr><tr><th>Other Details:- </th><td>:- " +
          "Address= "+addr+". PreschoolName= "+preschoolName+". Message= "+message +
          "</td></tr></table></body></html>"
      );
      fetch("https://mailapi.teenybeans.in/", {
        method: "POST",
        body: formData
      })
        .then(function(response) {
          response.json().then(function(data) {
            // console.log(data);
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
    }
    function sendWelcomeMail(sendEmail) {
      // ---------Mail sent-----------------
      console.log(sendEmail);
      let formData = new FormData();
      formData.append("sendMail", "true");
      formData.append("reciever", sendEmail);
      formData.append("sender", "Teenybeans");
      formData.append("senderMail", "no-reply@teenybeans.in");
      formData.append("subject", "Welcome to Teenybeans Preschool");
      formData.append(
        "message",
        "<html><body><h3>Welcome " +
          name +
          " to Teenybeans Preschool.<br>Thank you for contacting us.</h3><h3>Please <a href='https://teenybeans.in/pdf/TeenyBeansDigitalPreschoolSolution.pdf' download='https://teenybeans.in/pdf/TeenyBeansDigitalPreschoolSolution.pdf'>Download</a> Teenybeans Digital Preschool Solution.</h3><h3>Download Link:- https://teenybeans.in/pdf/TeenyBeansDigitalPreschoolSolution.pdf </h3><p>Also please do check out our <a href='https://teenybeans.in'>website</a> for more information. click <a href='https://teenybeans.in'>here to know more.</a><br>Thank you<br>-Team Teenybeans. </p></body></html>"
      );
      fetch("https://mailapi.teenybeans.in/", {
        method: "POST",
        body: formData
      })
        .then(function(response) {
          response.json().then(function(data) {
            console.log(data);
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
    }
    
</script>