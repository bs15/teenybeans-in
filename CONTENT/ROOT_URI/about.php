<link rel="canonical" href="https://teenybeans.in/about" />
<title>Affordable Preschool Solution in India </title>
<meta name="description" content="Teeny beans provides a 360 degree solution to help set up and operate an international preschool for a very affordable investment.">
<style type="text/css">
	.slide.kenburns{
			background-image:url('/images/web/parallax-1.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/web/parallax-1-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns ">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>About our company</h1>
					<span>WHO WE ARE</span>
				</div>
			</div>
	</div>
</section>
<!--- title section end -->

<!--- video section start -->
<section class="background-grey">
	<div class="container">

		<div class="row">
			<div class="col-lg-6">
				<h2>NEW PRESCHOOL</h2>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/eEGaiRGUxgM?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen loading="lazy"></iframe>
			</div>
			<div class="col-lg-6">
				<h2>EXISTING PRESCHOOL</h2>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/1vc-WFWJUCE?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen loading="lazy"></iframe>
			</div>
		</div>
	</div>
</section>
<!--- video section end -->

<!--- about section start -->
<section style="background-image:url('/images/web/general-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="heading-text heading-section">
					<h2 style="color: #fff;">INTEGRATED LEARNING CENTRE (ILC)</h2>
					<div style="color: #fff;"> An ILC is an institution carrying expertise in early childhood education from birth till preteens. It correctly identifies and administers age appropriate programs for kids aged 6m-9 yrs.<br><br>

					For the owners of an ILC, besides the comfort of having invested into a well-diversified business model with muliple value generating streams, it is the goodwill earned in running a highly respectable early learning centre, thatstrongly differentiates an ILC as a proposition.</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/5ei92WT1BHU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen loading="lazy"></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<!--- about section end -->

<!--- card section start -->
<section>
	<div class="container">
		<div class="row team-members team-members-shadow">
			<div class="col-lg-4">
				<div class="team-member">
					<div class="team-image">
					<img src="/images/web/preschool.jpg" alt="International Preschool" loading="lazy">
					</div>
					<div class="team-desc">
						<h3>An International Preschool</h3>
						<p>A preschool following an internationally recognized curriculum following UK's EYFS curriculum guidelines. Lays the foundation stone for early learning with several integrated programs that continue into a child's formative years.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="team-member">
					<div class="team-image">
					<img src="/images/web/teacher-training.jpg" alt="International Teacher Training Centre" loading="lazy">
					</div>
					<div class="team-desc">
						<h3>Teacher Training</h3>
						<p>A study centre for International Institute of Montessori Teacher Training offering.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="team-member">
					<div class="team-image">
					<img src="/images/web/afterschool.jpg" alt="After School Activity Centre" loading="lazy">
					</div>
					<div class="team-desc">
						<h3>After School Activity Centre</h3>
						<p>Beanstalk 3P, Writo, MaxBrain Abacus, Super Phonics & Cambridge Young Learners English- Programs for all ages and seamlessly integrated with the preschool program.</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!--- card section end -->

<!--- about section start -->
<section id="about-bottom-text">
	<div class="container">
		<div class="row">
			<div class="heading-text heading-section">
				<h2>WHAT MAKES TEENY BEANS INTERNATIONAL ?</h2>
			</div>
			<div class="col-lg-9">
					<div>
						<p>There are several reasons which make an Integrated Learning Centre an international early learning center of excellence.</p>
						<ul style="list-style-type: square;">
							<li>The only preschool curriculum with recognition from International Montessori Council (IMC), USA as well as International Montessori Society (IMS), USA.</li>
							<li>Adopts a British Framework (EYFS) for early childhood education as its core curriculum.</li>
							<li>Has adopted internationally renowned curriculum for phonics &amp; handwriting as part of its augmented curriculum offering.</li>
							<li>Prepares Global Citizens for 2050 and beyond &ndash; the only curriculum teaching French at the preschool level.</li>
						</ul>
						<p>We&rsquo;ve meticulously put in place an excellent curriculum for early childhood practitioners. We wish all our partners, our early childhood educators and our dear parents, Happy Schooling!</p>
					</div>
			</div>
			<div class="col-lg-3 img-responsive">
			<picture>      
				<source srcset="
				/images/web/teenybeans-international-curriculum.webp" type="image/webp">
				<source srcset="
				/images/web/teenybeans-international-curriculum.jpg" type="image/jpeg">
				<img src="/images/web/teenybeans-international-curriculum.webp" alt="Teeny Beans International Curriculum logo" loading="lazy">
			</picture>
			</div>
		</div>
	</div>
</section>
<!--- about section end -->
