
<title>Blog| Teeny Beans Preschool Solution</title>
<meta name="description" content="Get the best Preschool setup guide from us. It's never been such easy to open & run a successful preschool. Best guidance by Teeny Beans.">
<meta name="robots" content="noindex" />

<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns" style="background-image:url('/images/web/blog-cover.jpg');">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Teeny Beans Blogs</h1>
				</div>
				
			</div>
	</div>
</section>

<section id="page-content">
	<div class="container">
		<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
			<div class="post-item border">
			    <div class="post-item-wrap">
			        <div class="post-image">
			            <a href="#"> <img src="/images/blog/no-royalty-preschool.jpg" alt="Something" /> </a>
			        </div>
			        <div class="post-item-description">
			            <span class="post-meta-date">Jan 21, 2017</span>
			            <h2><a href="#">Standard post with a single image </a></h2>
			            <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>
			            <a class="item-link" href="#">Read More </a>
			        </div>
			    </div>
			</div>
			<div class="post-item border">
			    <div class="post-item-wrap">
			        <div class="post-image">
			            <a href="#"> <img src="/images/blog/no-royalty-preschool.jpg" alt="Something" /> </a>
			        </div>
			        <div class="post-item-description">
			            <span class="post-meta-date">Jan 21, 2017</span>
			            <h2><a href="#">Standard post with a single image </a></h2>
			            <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>
			            <a class="item-link" href="#">Read More </a>
			        </div>
			    </div>
			</div>
			<div class="post-item border">
			    <div class="post-item-wrap">
			        <div class="post-image">
			            <a href="#"> <img src="/images/blog/no-royalty-preschool.jpg" alt="Something" /> </a>
			        </div>
			        <div class="post-item-description">
			            <span class="post-meta-date">Jan 21, 2017</span>
			            <h2><a href="#">Standard post with a single image </a></h2>
			            <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>
			            <a class="item-link" href="#">Read More </a>
			        </div>
			    </div>
			</div>
			<div class="post-item border">
			    <div class="post-item-wrap">
			        <div class="post-image">
			            <a href="#"> <img src="/images/blog/no-royalty-preschool.jpg" alt="Something" /> </a>
			        </div>
			        <div class="post-item-description">
			            <span class="post-meta-date">Jan 21, 2017</span>
			            <h2><a href="#">Standard post with a single image </a></h2>
			            <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>
			            <a class="item-link" href="#">Read More </a>
			        </div>
			    </div>
			</div>

		</div>
	</div>
</section>

<section id="page-content">
  <div class="container">
    <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
      <?php 
          $sql = "SELECT * FROM PAGES WHERE `DOMAIN` = 'teenybeanspreschoolcurriculum.com' ORDER BY PAGE_ID DESC";
          $result = mysqli_query($link, $sql);
          if(mysqli_num_rows($result)>0){
            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
              $id = $row['PAGE_ID'];
              $domain = $row['DOMAIN'];
              $uri = $row['URI'];
              $slug = $row['SLUG'];
              $title = $row['TITLE'];
              $altTag = $row['ALT_TAG'];
              $dateUpdated = $row['DATE_UPDATED'];
              $meta = $row['META'];
              echo '<div class="post-item border">
                      <div class="post-item-wrap">
                          <div class="post-image">
                              <a href="'.$slug.'"> <img src="https://bscdn.sgp1.digitaloceanspaces.com/publicImages/'.$domain.$uri.$slug.'.jpg" alt="'.$altTag.'" /> </a>
                          </div>
                          <div class="post-item-description">
                              <span class="post-meta-date">'.$dateUpdated.'</span>
                              <h2><a href="'.$slug.'">'.$title.'</a></h2>
                              <p>'.$meta.'</p>
                              <a class="item-link" href="'.$slug.'">Read More </a>
                          </div>
                      </div>
                  </div>';
                // echo '<div class="col-sm-4 col-md-6 col-lg-2">
                // <a href="singleProduct?id='.$id.'"><img style="margin-bottom:5px; border: 2px solid;" src="/CONTENT/UPLOADS/PRODUCT/'.$id.'/'.$imgName.'"  width="90px" height="90px"></a>
                // </div>';
             }
          }else{
            // echo mysqli_error($link);
            echo '<div class="alert alert warning">Blog Posts are coming!</div>';
          }

        ?>
      

    </div>
  </div>
</section>