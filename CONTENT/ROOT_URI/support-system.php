<link rel="canonical" href="https://teenybeans.in/support-system" />
<title>Technology and Administration Support</title>
<meta name="description" content="Best support provided by Teeny Beans. Our technology is designed not only for partners also for parents.">
<style type="text/css">
.slide.kenburns{
	background-image:url('/images/web/support-from-teenybeans.jpg');
}
@media(max-width:450px){
	.slide.kenburns{
	background-image:url('/images/web/support-from-teenybeans-small.jpg');}
}
  .youtube {
    background-color: #000;
    margin-bottom: 30px;
    position: relative;
    padding-top: 56.25%;
    overflow: hidden;
    cursor: pointer;
  }
  .youtube img {
    width: 100%;
    top: -16.82%;
    left: 0;
    opacity: 0.7;
  }
  .youtube .play-button {
    width: 90px;
    height: 60px;
    background-color: #333;
    box-shadow: 0 0 30px rgba( 0,0,0,0.6 );
    z-index: 1;
    opacity: 0.8;
    border-radius: 6px;
  }
  .youtube .play-button:before {
    content: "";
    border-style: solid;
    border-width: 15px 0 15px 26.0px;
    border-color: transparent transparent transparent #fff;
  }
  .youtube img,
  .youtube .play-button {
    cursor: pointer;
  }
  .youtube img,
  .youtube iframe,
  .youtube .play-button,
  .youtube .play-button:before {
    position: absolute;
  }
  .youtube .play-button,
  .youtube .play-button:before {
    top: 50%;
    left: 50%;
    transform: translate3d( -50%, -50%, 0 );
  }
  .youtube iframe {
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
  }
</style>

<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Support System</h1>
					<span>Support System</span>
				</div>
			</div>
	</div>
</section>
<!--- title section end -->

<section class="p-b-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<img alt="UDAAN - Franchisee MIS platform" src="/images/web/udaan.jpg" class="img-responsive">
			</div>
			<div class="col-lg-7">
				<div class="heading-text heading-section mt-5">
					<h2>UDAAN</h2>
					<h4>MANAGEMENT INFORMATION SYSTEM</h4>
					<p>Our Franchisee MIS platform UDAAN provides a cutting edge support system for franchisees to assume complete operational control and achieve excellence in business. UDAAN provides our business partners a platform to handle enquiries, admissions, manage stock and inventory, academic management, order management and so much more!</p>
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="background-grey p-b-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7">
				<div class="heading-text heading-section text-right mt-5">
					<h2>SPARSH</h2>
					<h4>PARENT APP</h4>
					<p>Sparsh is an APP connecting parents with Teeny Beans' internationally recognized preschool curriculum. The APP provides all parents with instant access to the daily preschool curriculum - the content book, the workbook, curriculum mapped multi-media connect and daily lesson plans. Parents can also connect with their favorite teachers through Sparsh and manage their childs learning journey together with their favorite preschool.</p>
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
			<div class="col-lg-5">
				<div class="wrapper">
		          <div class="youtube" data-embed="7hlUelnYfwg">
		            <div class="play-button"></div>
		          </div>
		        </div>
				<!-- <img alt="SPARSH - Parent app for teaching kids at home" src="/images/web/Sparsh-the-parent-app.png" class="img-responsive"> -->
			</div>
		</div>
	</div>
</section>

<section class="p-b-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<img alt="Bheem - partner service portal" src="/images/web/udaan.jpg" class="img-responsive">
			</div>
			<div class="col-lg-7">
				<div class="heading-text heading-section mt-5">
					<h2>Bheem</h2>
					<h4>PARTNER SERVICE PORTAL</h4>
					<p>Through SPARSH, our international partner service portal, our partners have the perfect platform to communicate with their dedicated learning managers while tapping into the organization wide resources available to improve their understanding of the operating requirements of the industry they are a part of.<br><br>As part of the Infinity Service, partners are empowered through SPARSH to schedule tasks for ILC Managers ranging from admission counselling, enquiry handling, teacher and coordinator interviews, etc. Through SPARSH, partners can create and manage multiple centres and teams and post unlimited queries through SPARSH’s in-built query handler to remain deeply connect with their ILC Manager for any and every requirement of the school.
					</p>
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="background-grey p-b-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7">
				<div class="heading-text heading-section text-right mt-5">
					<h2>MARKETING PARTNER</h2>
					<h4>MARKETING PARTNER</h4>
					<p>Our entire revenue model is based on one premise alone – our partners succeed in their venture. So our marketing support systems and services tendered is not based on some idle promise aimed at just securing a sign up. Our online and offline marketing effort is geared towards maximizing our partners reach and brand equity – our fate is linked to your success, and this makes us that much more hungry for YOUR success!
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
			<div class="col-lg-5">
				<img alt="MARKETING PARTNER" src="/images/web/marketing-support.jpg" class="img-responsive">
			</div>
		</div>
		<div class="heading-text">
			<p>We work on an omni-channel strategy to maximize admissions for our partners -</p>
					<ul class="list-icon list-icon-check">
						<li><strong>Social Media -</strong>We have a robustly defined social media strategy to target relevant target segments to propagate your brand and garner more admissions.</li>
						<li><strong>Local Marketing -</strong>We work alongside you to sharpen your local marketing efforts to create and promote YOUR brand.</li>
						<li><strong>Local Search Optimization (online) -</strong> Our digital marketing expertise is leveraged to create a strong digital brand identity for our partners.</li>
						<li><strong>Web Presence -</strong> We manage your web presence locally through customized micro-sites and local listings to enhance your brand perception amongst local audiences.</li>
						<li><strong>Blog Sites -</strong>We believe our partners go on to become experts in early childhood education through active engagement with us. As a key part of a 360-degree digital strategy, we create the same impression through impressively written smart content around early childhood that reinforces that perception amongst your target customers.</li>
					</ul>
			</p>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<img alt="technology support" src="/images/web/technology-support.jpg" class="img-responsive">
			</div>
			<div class="col-lg-7">
				<div class="heading-text heading-section mt-5">
					<h2>TECHNOLOGY</h2>
					<h4>TECHNOLOGY</h4>
					<p>We are a technology leader in this space and our partners enjoy the benefit of being connected to their customers through web and mobile apps that has been custom made for their requirement by us.</p>
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="background-grey p-b-0">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7">
				<div class="heading-text heading-section text-right mt-5">
					<h2>TRAINING & DEVELOPMENT</h2>
					<h4>TRAINING & DEVELOPMENT</h4>
					<p>We offer the best induction program in the industry with a barrage of specially curated engaging e-learning modules that run into 100+ hours of induction training. We also conduct in-house on-site training and face to face training at our corporate office as part of the induction program for our various franchise programs.<br><br> Ongoing curriculum and administrative training is also tendered to our partners at the start of every term. With academics being the bedrock of The Beanstalkedu franchise opportunity, training our partners is something that is not just a necessity but also something we take a lot of pride in.</p>
					<!-- <a class="btn" href="#"><i class="fa fa-tint"></i> Learn more</a> -->
				</div>
			</div>
			<div class="col-lg-5"> <img alt="TRAINING & DEVELOPMENT" src="/images/web/training-development.jpg" class="img-responsive"> </div>
		</div>
	</div>
</section>
