<link href="/css/plugins.css" rel="stylesheet" async>
<link href="/css/style.css" rel="stylesheet" async>
<script>
    window.onload=(event)=>{
    var iframe = document.getElementById("gtm-iframe");
    iframe.src = "https://www.googletagmanager.com/ns.html?id=GTM-MFNWJVB" // here goes your url
    }
</script>
</head>
<body>

    <!-- Google Tag Manager (noscript) -->
<iframe src="about:blank" height="0" width="0" style="display:none;visibility:hidden" id="gtm-iframe"></iframe>
<!-- End Google Tag Manager (noscript) -->

<div class="body-inner">

<!--- header section start -->
<!-- <header id="header" data-transparent="true" class="dark header-logo-center"> -->
<header id="header" data-transparent="true" class="dark header-logo-center">
    <div class="header-inner">
        <div class="container">

            <div id="logo">
                <a href="/">
                    <span class="logo-default">
                        <img src="/images/web/teenybeans-logo.png" alt="logo" style="max-width: 75%; vertical-align: top;" loading="lazy">
                    </span>
                    <span class="logo-dark">
                        <img src="/images/web/teenybeans-logo.png" alt="logo" style="max-width: 75%; vertical-align: top;" loading="lazy">
                    </span>
                <!-- <span class="logo-dark">Teeny Beans</span> -->
                </a>
            </div>

            <!-- <div id="search">
                <a id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i class="icon-x"></i></a>
                <form class="search-form" action="#" method="get">
                    <input class="form-control" name="q" type="text" placeholder="Type & Search..." />
                    <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                </form>
            </div>

            <div class="header-extras">
                <ul>
                    <li>
                    <a id="btn-search" href="#"> <i class="icon-search"></i></a>
                    </li>
                    <li>
                        <div class="p-dropdown">
                            <a href="#"><i class="icon-globe"></i><span>EN</span></a>
                            <ul class="p-dropdown-content">
                                <li><a href="#">Bengali</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div> -->

            <div id="mainMenu-trigger">
                <a class="lines-button x"><span class="lines"></span></a>
            </div>

            <div id="mainMenu">
                <div class="container">
                    <nav>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/curriculum">Curriculum</a></li>
                            <li class="dropdown"><a href="/services">Services</a>
                                <ul class="dropdown-menu">
                                    <li class=""><a href="/support-system">Support System</a></li>
                                </ul>
                            </li>
                            <li><a href="/faq">FAQ</a></li>
                        </ul>

                        <ul>
                            <li><a href="/preschool-franchise-without-royalty">Franchise</a></li>
                            <li><a href="/preschool-at-home">Home Preschool</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>

</header>
<!--- header section end -->
