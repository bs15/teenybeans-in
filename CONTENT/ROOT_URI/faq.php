<link rel="canonical" href="https://teenybeans.in/faq" />
<title>How to open a preschool in India| Teeny Beans Preschool Solution</title>
<meta name="description" content="India’s best zero royalty international preschool chain in India. Everything you wanted to know about starting a world class preschool in India.">
<style type="text/css">
  .slide.kenburns{
			background-image:url('/images/web/faq-bg.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/web/faq-bg-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Frequently Asked Questions</h1>
					<span>Commonly asked questions about opening a Preschool</span>
				</div>
			</div>
	</div>
</section>
<!--- title section end -->



<section>
	<div class="container">
		<div class="tabs tabs-vertical">
		<div class="row">
			<div class="col-md-3">
				<ul class="nav flex-column nav-tabs" id="myTab4" role="tablist" aria-orientation="vertical">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#concept-faq" role="tab" aria-controls="home" aria-selected="true">Concept</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#services-faq" role="tab" aria-controls="profile" aria-selected="false">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#next-steps" role="tab" aria-controls="contact" aria-selected="false">Next Steps</a>
					</li>
				</ul>
			</div>
			<div class="col-md-9">
				<!-- <div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">...</div>
				<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
				<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
				<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
				</div> -->
				<div class="tab-content" id="myTabContent4">
					<div class="tab-pane fade show active" id="concept-faq" role="tabpanel" aria-labelledby="home-tab">
						<div class="accordion accordion-shadow">
							<div class="ac-item">
								<h5 class="ac-title">1. What is an individual-branded preschool?</h5>
								<div class="ac-content">
									<p>“It is a marketing strategy of branding different institutions by different names. Such a branding strategy helps establish a unique brand identity, image and positioning for an individual institute.”<br><br> It helps companies that use this strategy to target different market segments, i.e., consumer groups. So a preschool in Bannerghatta, Bangalore can use an individual branding approach to brand one’s institute as Tulip – House of Kids and run an independent and successful preschool establishment.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">2. What sort of branding do preschool chains like Kidzee or Eurokids or Kangaroo Kids employ?</h5>
								<div class="ac-content">
									<p>Individual branding contrasts with umbrella branding. Umbrella branding or corporate branding is a strategy of marketing all company’s products together.<br><br>Corporate branding includes using the same brand name and identity for its whole product range. So Euro Kids in Pimpri, Pune branded as Euro Kids Tulips would have Euro Kids as the corporate brand and Tulips as a sub-brand.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">3. Which of the two approaches – individual branding vs corporate branding is more relevant in the preschool space?</h5>
								<div class="ac-content">
									<p>For starters, majority of preschools in India are individual branded preschools. In the last decade or so, one would have seen a deluge of national & regional preschool brands flooding the market. <br><br>

              While a corporate branded preschool would work well in a set of markets where the target group of consumer is in sync with the brand ethos of the corporate brand, it is impossible to create a corporate brand that cuts across the different socio-economic-demographic strata that exists even within different neighbourhoods within a city, let alone different cities and different states of India. <br><br>

              So within the preschool space, there is more scope for individual branded preschool to thrive. And this is the norm internationally too. Thus it’s not just from a branding perspective alone. There are several other reasons why an individual branded preschool is more relevant than a corporate branded preschool. We will discuss more on that later.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">4. Can an individual-branded preschool run successfully?</h5>
								<div class="ac-content">
									<p>There is no direct way to answer this but in short, it depends! Another way to answer the question is with another question - can a corporate branded preschool run successfully?<br /><br> If you forget what form of branding a preschool has opted for and simply concentrate on the factors for success, you'd be able to pen the following winning formula-</p>
              <ul class="list-icon list-icon-check">
                <li>Have great child-friendly infrastructure in place.</li>
                <li>Have a great curriculum that's aligned to achieving early childhood developmental milestones.</li>
                <li>Implement a strict quality adherence framework to ensure the delivery of a great curriculum.</li>
                <li>Have all necessary learning aids and equipment necessary for the delivery of lesson plans</li>
                <li>Have a great motivated team</li>
                <li>Have super duper customer centricity</li>
                <li>Work hand in hand with local communities to build the credibility of your institute</li>
              </ul>
              <p>What a corporate branded preschool does well is that it provides a structure for a newbie entering the preschool industry. However, it is the <strong>execution capability</strong> of the newbie that will determine the success or failure of the preschool (forget the form of branding). And it is here that corporate branded preschool solutions fall short!<br /><strong>Despite execution capability being the lead determinant for success, there is hardly any commensurate time spent in capability development by preschool chains to begin with or in running of the preschool.</strong></p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">5. What is the solution proposed by Teeny Beans?</h5>
								<div class="ac-content">
									<p>At Teeny Beans, we help co-create individually branded preschools & early childhood learning centres of excellence.</p>
						              <p>What sets us apart from any other solution provider is the fact that our solution equips individuals to set up and operate a highly diversified business with multiple value streams, a solution which we call <strong>integrated learning centre</strong>.<br /><br />Specifically, an integrated learning centre is characterized by </p>
						              <ul  class="list-icon list-icon-check">
						              <li>An international preschool</li>
						              <li>An international teacher training institute</li>
						              <li>A kids afterschool activity centre</li>
						              </ul>
						              <p>Our solution is a holistic business solution for our partners and not a piecemeal solution. And most importantly our solution is a<strong>non-franchise zero royalty model<strong> </strong></strong>the only model we believe is relevant in the preschool space.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">6. Why is Teeny Beans an international curriculum?</h5>
								<div class="ac-content">
									<p>We have meticulously put in place an excellent curriculum for early childhood practitioners. And it is an international curriculum on a lot of counts.
					              <ul class="list-icon list-icon-check">
					              <li>The only preschool curriculum with recognition from<strong> International Montessori Council (IMC), USA, </strong>as well as<strong> International Montessori Society (IMS), USA</strong>.</li>
					              <li>It adopts a British Framework (EYFS) for early childhood education as its core curriculum.</li>
					              <li>It has adopted an internationally renowned curriculum for phonics & handwriting as part of its augmented curriculum offering. Prepares Global Citizens for 2050 and beyond & only curriculum teaching French at the preschool level.</li>
					              </ul></p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">7. How do we convince parents about our proposition when there are other branded schools offering preschool services in the neighbourhood ?</h5>
								<div class="ac-content">
									<p>The Teeny Beans Solution is based on the effective implementation of several strongly differentiated propositions that places each centre of learning at a significant advantage over other preschools in the area. Some of them are – <br><br>

            	<li>An&nbsp;<u>international</u>&nbsp;center of excellence with international affiliations.</li>
				<li>Augmented Programs being implemented that are borne out of&nbsp;<u>strong parental insights</u>&nbsp;and that is internationally validated.</li>
				<li>An&nbsp;<u>integrated curriculum</u>&nbsp;that places complete emphasis on child development in early childhood.</li>
				<li>Academics are at the heart of what we do &ndash; our teacher training institute drives home that advantage. Our institute produces teachers for other preschools and high schools.</li>

              You’ll be equipped to outshine competition by counselling effectively on several other aspects. We have 70 hours+ individual training session with you before you even lay a single brick at your premise. It would cover everything – from curriculum to preschool design layout, etc.
              By the time you set up your school you would talk the language of experts, making counselling more engaging. <br><br>

              Teeny  Beans also provides solid credibility to your institution as curriculum partners who are not just selling a solution but <u>co-implementing it with you</u>. Our early learning specialists are committed to help deliver exceptional results in implementing a
              world class curriculum while equipping you with necessary wherewithal to run an early childhood learning institute independently.<br><br>

              The presence of an international teacher training institute in the same premise automatically adds to the aura of your centre as a
              seat of learning. In time, the question of differentiation will be self evident.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">8. How is Teeny Beans different from a preschool franchisor ?</h5>
								<div class="ac-content">
									<p>For starters, no discussion post set up is ever going to be about commercials or royalties.
              It’s only going to be about the curriculum, delivery, training, recruitments, parental concerns, etc.
              Things that assume only peripheral significance for any preschool franchisor. This would resonate strongly with existing preschool franchise owners. <br><br>

              We offer a one-time investment with zero royalties and commissions. We co-implement the curriculum with you and co-administer the ILC along with you – forever. All the while, the attempt would be to make your brand resonate amongst your local community through strong marketing and rigorous implementation of a fantastic curriculum.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">9.  How did the Teeny Beans curriculum get developed ?</h5>
								<div class="ac-content">
									<p><b>Sugar and spice and everything nice!</b>
              Well it wasn’t as easy though! Several expert practitioners in early childhood education worked together over many
              cycles of the academic curriculum to gradually perfecting the teeny beans curriculum. Since we cover the entire spectrum
              from preschool to afterschool to teacher training, the enormity of the requirement was not lost on us. <br><br>

              We had disciplinary experts working on various aspects of the curriculum round the clock and a core committee of
              academicians worked on the assimilation and integration of best practices in each discipline to come to where we are today.
              It’s important to note that the same focus and fervor continues as we continue to keep perfecting finer aspects of our vast curriculum.<br><br>

              We followed a four stage process –<br><br>

              <b>Planning</b> - <br>Identifying key issues, trends and evidence based research of relevance in our context. Also assessed needs and issues that we
               wanted to address.<br><br>

               <b>Articulating and Developing–</b>  <br>Articulating our philosophy, defining curriculum goals, identifying resource materials and assessment
                items and instruments.<br><br>

                <b>Implementing</b> – <br> Putting the programs into practice.<br><br>

                <b>Evaluating –</b> <br>We constantly worked on upgrading various elements of the curriculum based on evidence presented through
                implementation at various learning centres. <br><br>

              The point to be noted here is that while the core part of the curriculum itself took a couple of years to take shape,
              it is the practical execution of the curriculum at various learning centres across the country over a period of five years that
              lent evidence to the effectiveness of the same.<br><br>

              Teeny Beans is the perfect example of a research based curriculum, implemented, evaluated and made execution ready for you!</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="services-faq" role="tabpanel" aria-labelledby="profile-tab">
						<div class="accordion accordion-shadow">
							<div class="ac-item">
								<h5 class="ac-title">10. What services do you provide to me if I want to start a preschool ?</h5>
								<div class="ac-content">
									<p>Why just open a preschool when you can open a preschool, a teacher training institute and a kid’s activity centre as an integrated learning centre ?<br><br>

              Our basket of services helps you to not just start an integrated learning centre but to successfully run it. While we have a more
              detailed break up of our services under the section on Our Services, briefly summarizing it here - <br><br>

              <b>a. PRE SET UP SERVICES :</b> <br>Training Program on Understanding the Business, Training on Financial Planning and Overhead
              Control & an in-depth Curriculum Training for ILC owner as well as all teaching staff.<br><br>

              <b>b. DURING SET UP :</b> <br>Site Selection, School Layout Designing, Learning Materials, Creating a Brand Identity for an
              individually branded preschool (both Offline & Digital) and finally Recruitments. <br><br>

              <b>c. IN-SCHOOL OPERATIONS :</b> <br>Administrative Support, Curriculum Implementation, Technical Support, Marketing Support
              through Digital Fortification and Enquiry Handling, Parent Engagements like post admission counselling and PTM feedback.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">11. What Training does Teeny Beans provide ?</h5>
								<div class="ac-content">
									<p>
										<p>
                Training is a continuous process. This is why we would be the only solution provider in this space that does not sever ties post-sales but in fact increases engagements once your school is up and running. Following are the touchpoints for training -
              </p>
              <ul class="list-icon list-icon-check">
                <li>
                  We provide a 3-day in-person intensive training in Kolkata in our model preschool Beanstalk International Preschool.
                </li>
                <li>
                  Preschool owners will also have a one-one skype session with our Co-Founder to understand business basics and his own professional journey in establishing a successful ILC.
                </li>
                <li>
                  We also have an e-learning module on the various aspects of the preschool set up and curriculum training that augments one's understanding.
                </li>
                <li>
                  Every new teacher or coordinator joining the preschool goes through the same e-learning module on curriculum training.
                </li>
              </ul>
									</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">12. Does Teeny Beans help in running my preschool ?</h5>
								<div class="ac-content">
									<p>This is the question which evokes a lot of expectation and subsequent disappointment for new preschool owners. Every brand and curriculum provider keeps promising a ‘yes’ with vague references to training and support to substantiate it. <br>

              So does Teeny Beans help in running your preschool ? YES! <br><br>

              We don’t measure our success in selling a solution to you. Our success is helping you establish your learning centre and achieve operational break even for you to begin with.<br><br>

              Here’s how we do things differently –
                <ul class="list-icon list-icon-check">
					<li>An <strong>experienced ILC manager</strong> is assigned for your preschool as your <strong>single touchpoint</strong> for any academic or administrative query. It&rsquo;s almost like having <strong>a preschool coordinator working&nbsp;</strong> remotely for you.</li>
					<li>We <strong>handle your inquiries&nbsp;</strong>for you &ndash; yes, you read it right! There are ways in which we could probably do a better job than you at this, at least in year 1. All the while you keep getting trained on living a life without us if that is what you&rsquo;d want after year 1.</li>
					<li>We assist you in <strong>teacher recruitments</strong>. With our network of teacher training institutes, we could try sourcing teachers for you as well. But we will certainly interview every teacher and ensure that the teacher is class-ready before deployment.</li>
					<li>Your ILC manager handles&nbsp;<strong>parent engagements</strong> for you which includes post-admission counseling and PTM feedback.</li>
					<li>Your ILC manager assists in curriculum audit to keep the preschool owner appraised regarding the quality of curriculum delivery.</li>
					<li>A <strong>digital marketing specialist</strong> helps create a web presence for your individual branded ILC and helps you stay relevant in search listings. <br /><br /></li>
				</ul>

              At Teeny Beans we take your trust very seriously. We go far beyond the mandate of any regular corporate brand or curriculum provider.
                We are committed to see your brand succeed and that’s our only metric for success.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">13. Do you charge royalty for your services ?</h5>
								<div class="ac-content">
									<p>We operate on a non-franchise, Zero Royalty mode. We help you build an everlasting individual branded ILC and expect we’d be doing enough to help you become the early learning expert practitioner that you had wanted to be.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">14. So what is your revenue model ?</h5>
								<div class="ac-content">
									<p>We charge you upfront fees and that’s good for a lifetime. There are several continuous services that we offer, and administrative and marketing support we continue to give to help you administer the Teeny Beans curriculum effectively. While those are not separately charged, we do charge for curriculum books that we provide.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">15. Do you provide bags and uniform ?</h5>
								<div class="ac-content">
									<p>We provide you all the artwork and specifications necessary to get these organized yourself. On special request, we do provide these for you but we encourage you to look for local options in your city.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">16. Do you manufacture any preschool learning aids or Montessori equipments ?</h5>
								<div class="ac-content">
									<p>No we don’t. What that does though is that it opens us up to two opportunities –
						               <ul class="list-icon list-icon-check">
						               <li>Source from the best quality manufacturer at the best possible price. We've done it over the years, this benefit gets passed on to you.</li>
						<li>Concentrate on what we do best create outstanding educational products and deliver outstanding customer service.</li>
						               </ul>
						              Each and every learning aid and equipment recommended and provided to you is mapped exactly to a unique aspect of the curriculum.
						              We have no desire or motivation to bring obsolescence into the ordering of redundant items that never get used just because we may
						              be manufacturing them!<br><br>

						              Neither are we wed to any particular vendor and hence retain complete flexibility in sourcing from the best option at a point of time.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="next-steps" role="tabpanel" aria-labelledby="contact-tab">
						<div class="accordion accordion-shadow">
							<div class="ac-item">
								<h5 class="ac-title">17. What marketing activities would be undertaken by Teeny Beans for my ILC ?</h5>
								<div class="ac-content">
									<p>All our efforts are around making you an independent edupreneur and your ILC a seat of learning acknowledged by your local community. We set up a search engine optimized micro-site for you and undertake various digital marketing and social media marketing activities and train you in sustaining these activities. There are several off-site seminars and presentations that an ILC can do to market their institute better.</p>
<p>Teeny Beans provides very high-quality marketing collaterals, presentations and individual branded videos that make these engagements synchronous to the high value associated with a Teeny Beans brand.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">18. Do I need to pay for curriculum upgrades each year ?</h5>
								<div class="ac-content">
									<p>The <strong>Knowledge Academy</strong> at The Beanstalk, which is the parent company of Teeny Beans, undertakes periodic revisions of an already excellent curriculum. The ILC does not pay for such revisions and is trained to administer it more effectively. If there are newer educational programs that get launched by The Beanstalk for a commercial licensing fee, an ILC can choose to evaluate it before implementing it at their institute. There is no compulsion to do so.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">19. A preschool, a teacher training institute and an afterschool activity centre – isn’t that too many things to do all together ?</h5>
								<div class="ac-content">
									<p>Well, the answer is yes, and no!</p>
<p>Despite the delivery of a very professional training program, implementing the solutions together for someone starting as a new preschool owner is a challenge. We recommend a calibrated approach with 6 months between the implementation of each leg of the tripod. Preschool first, followed by teacher training and afterschool centre. It helps in marketing each product well as well. The idea is that by the end of year 1, your centre is fully integrated learning centre par excellence.</p>
<p>For an existing preschool, with an existing franchise base of parents, it&rsquo;s easier to start with a preschool and a teacher training centre immediately. The kids' activity centre can be implemented in a 6m-1yr window.</p>
<p>The idea is not just to launch a product, but to make it work. We follow the same philosophy. We don&rsquo;t just sell you the solution, we implement it along with you!</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">20. What is the kind of area required to start an ILC ?</h5>
								<div class="ac-content">
									<p>While there are international stipulations valid for early learning centres in the developed world, there are no specific standards that are spelt out as a mandate in India. As a ballpark figure, anywhere upwards of 1200 sq feet carpet area is a good starting point.</p>
<p>The school's layout can be designed with ease with such space being available. Preferably one should have between 250-400 sq feet of open play area in the premise. Equally preferably, it should be a ground floor property.<br /><br /> Of course, the normal shifts radically when we discuss highly congested cities and expensive real estate. Do discuss with our experts on this matter before you sign up on a property for opening a new school.</p>
								</div>
							</div>
							<div class="ac-item">
								<h5 class="ac-title">21. What is the investment needed to start an ILC ? Can I opt to stagger my investment ?</h5>
								<div class="ac-content">
									<p>We have two standard products –<br><br>
              <b>PRIMERO – </b>Spanish for “first”. It’s exactly that. A solution for newbies in the industry. It entails implementation of the preschool curriculum for Playgroup and International Kindergarten 1. Recommended for the cautious and eternal perfectionist – perfect Primero before you move to the rest.<br><br>
              <b>MEGA -</b> MEGA, is the bouquet of programs that you are looking for if you already run a preschool and wish to upgrade your school to an ILC following the teeny beans model. We implement our preschool product for all age groups together and follow it up with a teacher training institute and an afterschool centre.<br><br>

              <b>Do get in touch with us to understand not just the cost structure but the financial modelling for both the options</b></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
<div class="space"></div>
</section>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is an individual-branded preschool?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is a marketing strategy of branding different institutions by different names. Such a branding strategy helps establish a unique brand identity, image and positioning for an individual institute. It helps companies that use this strategy to target different market segments, i.e., consumer groups. So a preschool in Bannerghatta, Bangalore can use an individual branding approach to brand one’s institute as Tulip – House of Kids and run an independent and successful preschool establishment."
    }
  }, {
    "@type": "Question",
    "name": "What sort of branding do preschool chains like Kidzee or Eurokids or Kangaroo Kids employ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Individual branding contrasts with umbrella branding. Umbrella branding or corporate branding is a strategy of marketing all company’s products together.
            Corporate branding includes using the same brand name and identity for its whole product range.
            So Euro Kids in Pimpri, Pune branded as Euro Kids Tulips would have Euro Kids as the corporate brand and Tulips as a sub-brand."
    }
  }, {
    "@type": "Question",
    "name": "Which of the two approaches – individual branding vs corporate branding is more relevant in the preschool space?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For starters, majority of preschools in India are individual branded preschools. In the last decade or so, one would have seen a deluge of national & regional preschool brands flooding the market. <br>
              While a corporate branded preschool would work well in a set of markets where the target group of consumer is in sync with the brand ethos of the corporate brand, it is impossible to create a corporate brand that cuts across the different socio-economic-demographic strata that exists even within different neighbourhoods within a city, let alone different cities and different states of India. <br>
              So within the preschool space, there is more scope for individual branded preschool to thrive. And this is the norm internationally too. Thus it’s not just from a branding perspective alone. There are several other reasons why an individual branded preschool is more relevant than a corporate branded preschool. We will discuss more on that later."
    }
  }, {
    "@type": "Question",
    "name": "Can an individual-branded preschool run successfully?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<p>There is no direct way to answer this but in short, it depends! Another way to answer the question is with another question - can a corporate branded preschool run successfully?<br />If you forget what form of branding a preschool has opted for and simply concentrate on the factors for success, you'd be able to pen the following winning formula-</p>
              <ul>
                <li>Have great child-friendly infrastructure in place.</li>
                <li>Have a great curriculum that's aligned to achieving early childhood developmental milestones.</li>
                <li>Implement a strict quality adherence framework to ensure the delivery of a great curriculum.</li>
                <li>Have all necessary learning aids and equipment necessary for the delivery of lesson plans</li>
                <li>Have a great motivated team</li>
                <li>Have super duper customer centricity</li>
                <li>Work hand in hand with local communities to build the credibility of your institute</li>
              </ul>
              <p>What a corporate branded preschool does well is that it provides a structure for a newbie entering the preschool industry. However, it is the <strong>execution capability</strong> of the newbie that will determine the success or failure of the preschool (forget the form of branding). And it is here that corporate branded preschool solutions fall short!<br /><strong>Despite execution capability being the lead determinant for success, there is hardly any commensurate time spent in capability development by preschool chains to begin with or in running of the preschool.</strong></p>"
    }
  },
   {
    "@type": "Question",
    "name": "What is the solution proposed by Teeny Beans?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text":"<p>At Teeny Beans, we help co-create individually branded preschools & early childhood learning centres of excellence.</p>
              <p>What sets us apart from any other solution provider is the fact that our solution equips individuals to set up and operate a highly diversified business with multiple value streams, a solution which we call <strong>integrated learning centre</strong>.<br /><br />Specifically, an integrated learning centre is characterized by </p>
              <ul>
              <li>An international preschool</li>
              <li>An international teacher training institute</li>
              <li>A kids afterschool activity centre</li>
              </ul>
              <p>Our solution is a holistic business solution for our partners and not a piecemeal solution. And most importantly our solution is a<strong>non-franchise zero royalty model<strong> </strong></strong>the only model we believe is relevant in the preschool space.</p>"}
    }, {
    "@type": "Question",
    "name": "Why is Teeny Beans an international curriculum?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<p>We have meticulously put in place an excellent curriculum for early childhood practitioners. And it is an international curriculum on a lot of counts.</p>
              <ol>
              <li>The only preschool curriculum with recognition from<strong> International Montessori Council (IMC), USA, </strong>as well as<strong> International Montessori Society (IMS), USA</strong>.</li>
              <li>It adopts a British Framework (EYFS) for early childhood education as its core curriculum.</li>
              <li>It has adopted an internationally renowned curriculum for phonics & handwriting as part of its augmented curriculum offering. Prepares Global Citizens for 2050 and beyond & only curriculum teaching French at the preschool level.</li>
              </ol>"
    }
  }, {
    "@type": "Question",
    "name": "How do we convince parents about our proposition when there are other branded schools offering preschool services in the neighbourhood ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<p>There is no direct way to answer this but in short, it depends! Another way to answer the question is with another question - can a corporate branded preschool run successfully?<br />If you forget what form of branding a preschool has opted for and simply concentrate on the factors for success, you'd be able to pen the following winning formula-</p>
              <ul>
                <li>Have great child-friendly infrastructure in place.</li>
                <li>Have a great curriculum that's aligned to achieving early childhood developmental milestones.</li>
                <li>Implement a strict quality adherence framework to ensure the delivery of a great curriculum.</li>
                <li>Have all necessary learning aids and equipment necessary for the delivery of lesson plans</li>
                <li>Have a great motivated team</li>
                <li>Have super duper customer centricity</li>
                <li>Work hand in hand with local communities to build the credibility of your institute</li>
              </ul>
              <p>What a corporate branded preschool does well is that it provides a structure for a newbie entering the preschool industry. However, it is the <strong>execution capability</strong> of the newbie that will determine the success or failure of the preschool (forget the form of branding). And it is here that corporate branded preschool solutions fall short!<br /><strong>Despite execution capability being the lead determinant for success, there is hardly any commensurate time spent in capability development by preschool chains to begin with or in running of the preschool.</strong></p>"
    }
  }, {
    "@type": "Question",
    "name": "How is Teeny Beans different from a preschool franchisor ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For starters, no discussion post set up is ever going to be about commercials or royalties.
              It’s only going to be about the curriculum, delivery, training, recruitments, parental concerns, etc.
              Things that assume only peripheral significance for any preschool franchisor. This would resonate strongly with existing preschool
              franchise owners. <br>
              We offer a one-time investment with zero royalties and commissions. We co-implement the curriculum with you and co-administer the ILC along with you – forever. All the while, the attempt would be to make your brand resonate amongst your local community through strong marketing and rigorous implementation of a fantastic curriculum."
    }
  }, {
    "@type": "Question",
    "name": "How did the Teeny Beans curriculum get developed ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<p><b>Sugar and spice and everything nice!</b></p>
              Well it wasn’t as easy though! Several expert practitioners in early childhood education worked together over many
              cycles of the academic curriculum to gradually perfecting the teeny beans curriculum. Since we cover the entire spectrum
              from preschool to afterschool to teacher training, the enormity of the requirement was not lost on us. <br><br>
              We had disciplinary experts working on various aspects of the curriculum round the clock and a core committee of
              academicians worked on the assimilation and integration of best practices in each discipline to come to where we are today.
              It’s important to note that the same focus and fervor continues as we continue to keep perfecting finer aspects of our vast curriculum.<br><br>
              We followed a four stage process –<br><br>
              <b>Planning</b> - <br>Identifying key issues, trends and evidence based research of relevance in our context. Also assessed needs and issues that we
               wanted to address.<br><br>
               <b>Articulating and Developing–</b>  <br>Articulating our philosophy, defining curriculum goals, identifying resource materials and assessment
                items and instruments.<br><br>
                <b>Implementing</b> – <br> Putting the programs into practice.<br><br>
                <b>Evaluating –</b> <br>We constantly worked on upgrading various elements of the curriculum based on evidence presented through
                implementation at various learning centres. <br><br>
              The point to be noted here is that while the core part of the curriculum itself took a couple of years to take shape,
              it is the practical execution of the curriculum at various learning centres across the country over a period of five years that
              lent evidence to the effectiveness of the same.<br><br>
              Teeny Beans is the perfect example of a research based curriculum, implemented, evaluated and made execution ready for you!"
    }
  }, {
    "@type": "Question",
    "name": "What services do you provide to me if I want to start a preschool ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Why just open a preschool when you can open a preschool, a teacher training institute and a kid’s activity centre as an integrated learning centre ?<br><br>
              Our basket of services helps you to not just start an integrated learning centre but to successfully run it. While we have a more
              detailed break up of our services under the section on Our Services, briefly summarizing it here - <br><br>
              <b>a. PRE SET UP SERVICES :</b> <br>Training Program on Understanding the Business, Training on Financial Planning and Overhead
              Control & an in-depth Curriculum Training for ILC owner as well as all teaching staff.<br><br>
              <b>b. DURING SET UP :</b> <br>Site Selection, School Layout Designing, Learning Materials, Creating a Brand Identity for an
              individually branded preschool (both Offline & Digital) and finally Recruitments. <br><br>
              <b>c. IN-SCHOOL OPERATIONS :</b> <br>Administrative Support, Curriculum Implementation, Technical Support, Marketing Support
              through Digital Fortification and Enquiry Handling, Parent Engagements like post admission counselling and PTM feedback."
    }
  }, {
    "@type": "Question",
    "name": "What Training does Teeny Beans provide ?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "<p>
                Training is a continuous process. This is why we would be the only solution provider in this space that does not sever ties post-sales but in fact increases engagements once your school is up and running. Following are the touchpoints for training -
              </p>
              <ul>
                <li>
                  We provide a 3-day in-person intensive training in Kolkata in our model preschool Beanstalk International Preschool.
                </li>
                <li>
                  Preschool owners will also have a one-one skype session with our Co-Founder to understand business basics and his own professional journey in establishing a successful ILC.
                </li>
                <li>
                  We also have an e-learning module on the various aspects of the preschool set up and curriculum training that augments one's understanding.
                </li>
                <li>
                  Every new teacher or coordinator joining the preschool goes through the same e-learning module on curriculum training.
                </li>
              </ul>"
     }
    }]
  }
</script>
