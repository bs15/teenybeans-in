<link rel="canonical" href="https://teenybeans.in/curriculum" />
<title>Best Preschool Curriculum in India | EYFS Curriculum</title>
<meta name="description" content="The best play school curriculum providers in India? Teeny Beans adopts a British curriculum (EYFS) and several acclaimed international programs for preschool kids. ">
<style type="text/css">
	.slide.kenburns{
			background-image:url('/images/slider/slider6.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/slider/slider6-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Best Preschool Curriculum</h1>
					<span>Learn about our international curriculum</span>
				</div>
			</div>
	</div>
</section>
<!--- title section end -->


<!--- about section start -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="heading-text heading-section">
					<!-- <h2>Non Franchise Preschool</h2> -->
					<picture>      
					<source srcset="
					/images/web/teenybeans-international-preschool-curriculum.webp" type="image/webp">
					<source srcset="
					/images/web/teenybeans-international-preschool-curriculum.jpg" type="image/jpeg">
					<img src="/images/web/teenybeans-international-preschool-curriculum.webp" alt="Teeny Beans international preschool curriculum" class="img-responsive" loading="lazy">
					</picture>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="row">
					<div class="col-lg-6">Teeny Beans international preschool curriculum has been carefully curated over years of research and implemented at Beanstalk International Preschool. Various aspects of our curriculum enjoy the advantage of international validation since it follows curriculum guidelines that have been set by international curriculum experts and qualified early childhood practitioners. Courtesy our affiliation with international early childhood institutes of repute like International Montessori Society (IMS) and International Montessori Council (IMC), we are also able to benchmark our curriculum to international standards and compare learning objectives & outcomes.</div>
					<div class="col-lg-6">While our international orientation gives us a distinctive advantage, what really sets us apart is the fact that our programs have been culturally contextualized and is relatable to a child’s everyday experiences. Parental insights have been captured methodically and that forms the basis for incorporating various enrichment programs that are a part of the augmented curriculum.</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--- about section end -->

<!--- eyfs intro section start -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="heading-text heading-section">
					<h2>THE CORE CURRICULUM</h2>
				</div>
			</div>
			<div class="col-lg-8">
					We follow the British Curriculum in adopting the Early Years Foundation Stage (EYFS) framework. The EYFS framework approaches early years' learning as a holistic, integrated focus on children’s creative expression, physical and communication skills as well as emotional and social development. Learning is carefully structured yet allows for exploratory, child-initiated experiences.
			</div>
		</div>
	</div>
		<div class="container">
		<div class="heading-text heading-line text-center">
			<h4>EYFS Principles</h4>
		</div>
		<div class="row team-members team-members-shadow">
			<div class="col-lg-3">
				<div class="team-member">
					<div class="team-image">
					<picture class="team-image">      
					<source srcset="
					/images/web/eyfs-unique-child.webp" type="image/webp">
					<source srcset="
					/images/web/eyfs-unique-child.jpg" type="image/jpeg">
					<img src="/images/web/eyfs-unique-child.webp" alt="A Unique Child" loading="lazy">
					</picture>
					</div>
					<div class="team-desc">
						<h3>A Unique Child</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="team-member">
					<div class="team-image">
					<picture class="team-image">      
					<source srcset="
					/images/web/eyfs-positive-relationship.webp" type="image/webp">
					<source srcset="
					/images/web/eyfs-positive-relationship.jpg" type="image/jpeg">
					<img src="/images/web/eyfs-positive-relationship.webp" alt="A Positive Relationship" loading="lazy">
					</picture>
					</div>
					<div class="team-desc">
						<h3>A Positive Relationship</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="team-member">
					<div class="team-image">
					<picture class="team-image">      
					<source srcset="
					images/web/eyfs-enabling-environment.webp" type="image/webp">
					<source srcset="
					images/web/eyfs-enabling-environment.jpg" type="image/jpeg">
					<img src="/images/web/eyfs-enabling-environment.webp" alt="Enabling Environment" loading="lazy">
					</picture>
					</div>
					<div class="team-desc">
						<h3>Enabling Environment</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="team-member">
					<div class="team-image">
					<picture class="team-image">      
					<source srcset="
					/images/web/eyfs-learning-development.webp" type="image/webp">
					<source srcset="
					/images/web/eyfs-learning-development.jpg" type="image/jpeg">
					<img src="/images/web/eyfs-learning-development.webp" alt="Learning and Development" loading="lazy">
					</picture>
					</div>
					<div class="team-desc">
						<h3>Learning | Development</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--- eyfs intro section end -->

<!--- eyfs learning area section start -->
<section style="padding-bottom: 10px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="heading-text heading-section">
					<h2>EYFS Areas of Learning</h2>
				</div>
				<p>Learning and development shape the activities and experiences that childcare providers offer children under the age of 5. The EYFS states that the educational programme offered must involve activities and experiences that cover 7 significant and inter-connected areas of learning and development. We’ll now take a brief look at each of these 7 areas and why they are important.</p>
			</div>
			<div class="col-lg-5">
				<img src="/images/web/eyfs-areas-of-learning.jpg" class="img-responsive" alt="EYFS Learning Circle" loading="lazy">
			</div>
		</div>
	</div>
</section>
<!--- eyfs learning area section end -->

<!--- timeline section start -->
<section id="page-content" class="no-padding">
	<div class="container">
		<div class="row justify-content-center">
			<div class="content col-lg-9">
				<ul class="timeline">
					<li class="timeline-item">
						<div class="timeline-icon">1</div>
						<h4>Personal, social, and emotional development</h4>
						<!-- <div class="timeline-item-date">21 January, 2020</div> -->
						<p>This area helps to shape children’s social skills and develops respect and an understanding of their different feelings.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">2</div>
						<h4>Communication and language development</h4>
						<p>Providing an environment for young children to express themselves and speak and listen in a range of situations allows them to develop their language and communication skills.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">3</div>
						<h4>Physical development</h4>
						<p>We all know that young children often love to be active, but they also need to understand that continued physical activity as well as healthy food choices are important, and why.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">4</div>
						<h4>Literacy development</h4>
						<p>It’s important for children to discover phonemic awareness – the ability to hear and identify different words and sounds, and also to start reading and writing.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">5</div>
						<h4>Mathematics</h4>
						<p>Children need to be guided in developing skills with numbers and calculations, as well as being able to describe shapes, spaces, and measures.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">6</div>
						<h4>Understanding the world</h4>
						<p>This involves children making sense of things by observing and exploring everything from the places they spend time to the technology and other things that they use.</p>
					</li>
					<li class="timeline-item">
						<div class="timeline-icon">7</div>
						<h4>Expressive arts and design</h4>
						<p>Activities like drawing, playing with paint, instruments or technology all give children the chance to express themselves and learn new things.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--- timeline section end -->


<!--- augmented curriculum section start -->
<section>
	<div class="container">
		<div class="row">
			<div class="heading-text heading-section text-center">
				<h2>THE AUGMENTED CURRICULUM</h2>
				<div>The augmented curriculum of Teeny Beans has been painstakingly capturing several key parental insights. While our core curriculum takes care of every aspect of developmental input as required in early childhood education, our augmented curriculum goes much deeper into some aspects that are considered as critical from a parental perspective. The parental insights cut across geographies and cultures and are extremely relevant. So the key question is</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="content col-lg-9">
				<div class="carousel arrows-visibile testimonial testimonial-single" data-items="1" data-loop="true" data-autoplay="true" data-autoplay="3500" data-arrows="true" data-animate-in="fadeIn" data-animate-out="fadeOut">
					<div class="testimonial-item">
						<img src="/images/web/writo.jpg" alt="Writo - A handwriting program for kids" loading="lazy">
						<h3>Hand Writing</h3>
						<span>Writo</span>
						<span>A comprehensive handwriting program for pre-schoolers and beyond. As opposed to traditional visual copy technique proposed through the 'trace and learn' method, Writo adopts the scientifically progressive directed or movement based learning that involves multiple sense organs to create lasting neural pathways.</span>
					</div>
					<div class="testimonial-item">
						<img src="/images/web/minimax.jpg" alt="Minimax - brain developmental program for children" loading="lazy">
						<h3>Brain Development</h3>
						<span>Mini Max</span>
						<span>A whole brain developmental program aimed at developing whole brain thinkers very early in a child’s learning curve. Right brain activation from the preschooling age helps leverage the full potential of our brain. Minimax Abacus does that and more.</span>
					</div>
					<div class="testimonial-item">
						<img src="images/web/super-phonics.jpg" alt="Super Phonics - English Communication program for children" loading="lazy">
						<h3>English Communication</h3>
						<span>Super Phonics</span>
						<span>A complete phonics curriculum for preschool, develops a strong foundation for reading and writing. It uses the synthetic phonics method of teaching the letter sounds in a way that is fun and multi-sensory. Children learn how to use the letter sounds to read and write words.</span>
					</div>
					<div class="testimonial-item">
						<img src="/images/web/bonjour.jpg" alt="Bonjour - Accelerated Cognitive Development" loading="lazy">
						<h3>Accelerated Cognitive Development</h3>
						<span>Bonjour</span>
						<span>Children are natural language “sponges” and learn easily through repetition and play-not analysis. Evidence shows that early exposure to foreign languages helps develop nearnative pronunciation, raises test scores in all areas, helps children to develop a global view and cultural understanding, and allows for a longer sequence of study and higher levels of language proficiency.</span>
					</div>
				</div>
			</div>
		</div>
		<hr class="line">
		<p class="text-center">The Augmented Programs have been carefully curated through years of research by the Knowledge Academy at TBPC, are designed age-appropriately and are cutting edge in modern education. Miles ahead of any traditional preschool curriculum!</p>
	</div>
</section>
<!--- augmented curriculum section end -->
