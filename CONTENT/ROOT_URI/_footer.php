<!--- footer section start -->
<footer id="footer" class="inverted">
    <div class="footer-content">
      <div class="container">
        <div class="row">

          <div class="col-xl-4 col-lg-4 col-md-4">
            <p><a href="#"><img src="/images/web/teenybeans-logo1.png" style="max-width:200px" alt="teenybeans logo"></a></p>
            <p>Teeny Beans is a non-franchise and zero-royalty based holistic solution for new and existing practitioners in the space of early childhood education.</p>
            <h4><strong>Download SPARSH</strong></h4>
            <a id="gplaybutton" href='https://play.google.com/store/apps/details?id=com.beanstalkedu.app.sparsh&utm_source=website&utm_campaign=tb_web&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img class="img-responsive" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
          </div>

          <div class="col-xl-4 col-lg-4 col-md-4">
            <div class="widget  widget-contact-us" style="background-image: url('/images/web/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat">
            <h4>Get in Touch with Us</h4>
              <ul class="list-icon">
                <li><i class="fa fa-map-marker-alt"></i> DE 2B, VIP Rd, Desh Bandhu Nagar,Baguiati<br>
                Rajarhat, West Bengal 700059</li>
                <li><i class="fa fa-phone"></i> +91 97483 86431</li>
                <li><i class="far fa-envelope"></i> <a href="#">enquiry@beanstalkedu.com</a> </li>
                <li> <br>
                <i class="far fa-clock"></i>Monday - Saturday: <strong>09:00 - 18:00</strong> <br>
                Sunday: <strong>Closed</strong> </li>
              </ul>
            </div>
          </div>
          <a href="/DOCS/Preschool-Business-Proposal.pdf" id="dLink" download style="display:none;"><i class="fa fa-envelope"></i> Download Corporate Brochure</a>

          <div class="col-xl-4 col-lg-4 col-md-4">

            <div class="widget clearfix widget-newsletter">
              <h4 class="widget-title"></i> Our Newest Partners</a></h4>
              <ul class="list-icon partner-sites">
                <li><i class="fa fa-check"></i> <a href="/partners/nest-berhampore-westbengal/">Nest International Preschool</a></li>
                <li><i class="fa fa-check"></i> <a href="/partners/steppingstones-nagpur-maharashtra/">Stepping Stones International Preschool</a></li>
                <li><i class="fa fa-check"></i> <a href="/partners/bizzybees-vadodara-gujarat/">Bizzy Bees International Preschool</a></li>
              </ul>
              <h4 class="widget-title"></i> Download Corporate Brochure</a></h4>
              <p>Get a look at our offerings.</p>
              <p id="thankMsg" style="display: none;">Thank you! Please check your email for the brochure.</p>
              <form class="widget-subscribe-form p-r-40" action="#" role="form" method="post" novalidate="novalidate">
                <div class="input-group">
                  <input aria-required="true" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" type="email" id="email2">
                  <span class="input-group-btn">
                  <button  id="widget-subscribe-submit-button dBtn" onclick="downloadBr();" class="btn"><i class="fa fa-paper-plane"></i></button>
                   </span>
                </div>
              </form>
              <p id="emailError2" style="display: none; color: red;" >Please Provide a valid email</p>
            </div>
          </div>


        </div>
      </div>
    </div>
    <div class="copyright-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">

            <div class="social-icons social-icons-colored">
              <ul>
                <li class="social-facebook"><a href="https://www.facebook.com/teenybeanspreschoolcurriculum/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li class="social-youtube"><a href="https://www.youtube.com/channel/UC4ppJKZgGaOxexol-zMwZDQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <li class="social-twitter"><a href="https://twitter.com/teeny_beans" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li class="social-instagram"><a href="https://www.instagram.com/teenybeans.in/" target="_blank"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>

          </div>
          <div class="col-lg-6 text-right">
            <div class="copyright-text">
              <a href="/terms-and-conditions">Terms & Conditions</a> | 
            © 2020 Teeny Beans | All Rights Reserved. </div>
          </div>
        </div>
      </div>
    </div>
</footer>
<!--- footer section end -->


</div>

<a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
<script type="text/javascript">
  function downloadBr() {
    var email = document.getElementById("email2").value;
    var emailError2 = document.getElementById("emailError2");
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (email == '') {
      emailError2.style.display = "block";
    }else{
      if (reg.test(email) == false) 
      { 
          emailError2.style.display = "block";
      }else{
        emailError2.style.display = "none";
        document.getElementById("email2").value = '';
        document.getElementById("thankMsg").style.display = "block";
        saveEmail(email);
        sendEmail(email);
      }
    }
    
    // document.getElementById("thankMsg").style.display = "block";
    

  }
  var message = '<html><body>Welcome to Teenybeans. You have showed interest to know more about us and here it is. You will find our Brochure Link in this mail. Download the brochure and know everything about us. <br>And for more information you can visit our website <a link="https://teenybeans.in">here</a>.<br><a href="https://teenybeans.in/DOCS/Preschool-Business-Proposal.pdf" download>Download Your Brochure Now!</a><br>-Team Teenybeans</body></html>';
  function sendEmail(email) {
    let formData = new FormData();
      formData.append('sendMail', 'true');
      formData.append('reciever', email);
      formData.append('sender', 'Teenybeans');
      formData.append('senderMail', 'no-reply@teenybeans.in');
      formData.append('subject', "Teenybeans Brochure");
      formData.append('message', message);
      fetch("https://mailapi.teenybeans.in/", {
            method: "POST",
            body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
  }
  function saveEmail(email) {
    let formData = new FormData();
    formData.append('formName', 'teenyBeansBrochure');
    formData.append('Email', email);
    // formData.append('Address', addr);
    fetch('https://api.teenybeans.in/API/contactFormProcessor/v1/', {
      method: 'POST',
      body: formData
    })
    .then(res => res.json())
    .then(json =>  {
      // window.location.href = "contact-back";
      }
    );
  }
</script>



<script src="/js/jquery.js"></script>


<script src="/js/plugins.js"></script>

<script src="/js/functions.js"></script>
<script type="text/javascript">
  ( function() {

  var youtube = document.querySelectorAll( ".youtube" );
  
  for (var i = 0; i < youtube.length; i++) {
    
    var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
    
    var image = new Image();
        image.src = source;
        image.addEventListener( "load", function() {
          youtube[ i ].appendChild( image );
        }( i ) );
    
        youtube[i].addEventListener( "click", function() {

          var iframe = document.createElement( "iframe" );

              iframe.setAttribute( "frameborder", "0" );
              iframe.setAttribute( "allowfullscreen", "" );
              iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

              this.innerHTML = "";
              this.appendChild( iframe );
        } );  
  };
  
} )();
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e32b4188e78b86ed8abce91/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>

</html>