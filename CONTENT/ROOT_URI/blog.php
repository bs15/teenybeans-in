<link rel="canonical" href="https://teenybeans.in/blog" />
<title>Blog| Teeny Beans Preschool Solution</title>
<meta name="description" content="Get the best Preschool setup guide from us. It's never been such easy to open & run a successful preschool. Best guidance by Teeny Beans.">

<?php 
  $link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $link->set_charset("utf8");
  if ($link->connect_error) echo $link->connect_error;
  
 ?>
<style type="text/css">
  .hovergreen :hover {
    color: #A3CB37;
  }
	.slide.kenburns{
			background-image:url('/images/web/parallax-1.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/web/parallax-1-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>


<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Teeny Beans Blogs</h1>
				</div>
			</div>
	</div>
</section>

<div class="container" style="padding-top: 20px;">
	<div class="row">
       <?php 
          $sql = "SELECT * FROM PAGES WHERE `DOMAIN` = 'teenybeanspreschoolcurriculum.com' ORDER BY PAGE_ID DESC";
          $result = mysqli_query($link, $sql);
          if(mysqli_num_rows($result)>0){
            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){ 
              $id = $row['PAGE_ID'];
              $domain = $row['DOMAIN'];
              $uri = $row['URI'];
              $slug = $row['SLUG'];
              $title = $row['TITLE'];
              $meta = $row['META'];
              echo '<div class="col-sm-12 col-md-6 col-lg-4" style="padding-bottom:20px;"><div class="card h-100" style="width:100%;border:none; border-bottom: 5px solid #A3CB37;  cursor: pointer;">
              <a href="'.$slug.'"><img src="https://bscdn.sgp1.digitaloceanspaces.com/publicImages/'.$domain.$uri.$slug.'.jpg" alt="'.$title.'" loading="lazy" style="height:250px; width:100%;"></a>
              <div style="height:250px;" class="card-body">
                <a class="hovergreen" href="'.$slug.'"><p class="" style="text-decoration:none;font-size:17px; font-weight:bold;">'.$title.'</p></a>
                <a href="'.$slug.'"><p>'.$meta.'</p></a>
              </div>
                <div class="card-footer" style="border:none;">
                  <a href="'.$slug.'" style="position:relative; margin-bottom:10px;" class="btn btn-outline-success">Read More</a>
                </div>
            </div></div>';
                // echo '<div class="col-sm-4 col-md-6 col-lg-2">
                // <a href="singleProduct?id='.$id.'"><img style="margin-bottom:5px; border: 2px solid;" src="/CONTENT/UPLOADS/PRODUCT/'.$id.'/'.$imgName.'"  width="90px" height="90px"></a>
                // </div>';
             }
          }else{
            // echo mysqli_error($link);
            echo '<div class="alert alert warning">Blog Posts are coming!</div>';
          }

        ?>

      </div>
</div>
<!--- title section end -->

