<link rel="canonical" href="https://teenybeans.in/services" async/>
<title>Best Preschool Solution in India </title>
<meta name="description" content="Thinking of how to open a preschool? Teeny Beans offers customized preschool set up solutions. Much better than taking any franchise of a preschool.">
<style type="text/css">
	.slide.kenburns{
			background-image:url('/images/slider/slider5.jpg');
		}
	@media(max-width:480px){
		.slide.kenburns{
			background-image:url('/images/slider/slider5-small.jpg');
		}
	}
</style>
<?php include("_menu.php");?>

<!--- title section start -->
<section id="slider" class="inspiro-slider dots-creative" data-height-xs="360">
	<div class="slide kenburns">
		<div class="bg-overlay"></div>
			<div class="container">
				<div class="page-title text-center text-light">
					<h1>Our Services</h1>
					<span>The services that we provide</span>
				</div>
			</div>
	</div>
</section>
<!--- title section end -->

<!--- existing preschool section start -->
<section>
	<div class="container">
		<div class="row">

			<div class="col-lg-6">
				<div class="heading-text heading-section">
					<h2 style="text-transform: uppercase;">For Existing Preschool</h2>
				</div>
				The Teeny Beans preschool solution is a customized package of solutions for anyone or any institute looking to start a preschool or upgrading an existing preschool. The series of explainer videos explain the steps that you would take in starting or revamping a preschool and how we go far beyond being just curriculum providers – we provide a solution and then implement it for you!
			</div>
			<div class="col-lg-6">
				<iframe width="560" loading="lazy" height="315" src="https://www.youtube.com/embed/yCk_qjyy7PU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section>
<!--- existing preschool section end -->


<!--- new preschool section start -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="heading-text heading-section text-center">
				<h2 style="text-transform: uppercase;">New Preschool</h2>
				<div>New preschool owners have the opportunity to learn how to operate a localized & valued seat of early childhood learning through our experience as integrated learning service providers. It de-risks business and provides multiple value streams for business owners to activate while delivering holistic learning within a localized community. The Zero to One service package is the set of services that we provide (and you require) that helps create the ideal early childhood educational institute you’ve always dreamt of. Opening an ILC is only half the work done. Anyone who has run a school knows that. Yet, all franchisors usually do the disappearing act round about now! Learn how our NO ROYALTY, INDIVIDUAL BRANDED ILC MODEL is way different and to YOUR ADVANTAGE!</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<iframe width="560" height="315" loading="lazy" src="https://www.youtube.com/embed/mBOWmd1gbBk?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<h3>From Zero to One</h3>
				<span>From ideation to creation</span>
				<ul class="list-icon list-icon-check list-icon-colored">
					<li>Thinking of setting up a preschool ?</li>
					<li>Thinking of upgrading your existing preschool ?</li>
				</ul>
			</div>
			<div class="col-lg-6">
				<iframe width="560" height="315" loading="lazy" src="https://www.youtube.com/embed/6YpjWN3ZXU4?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<h3>From One to Infinity</h3>
				<span>From creation to execution</span>
				<ul class="list-icon list-icon-check list-icon-colored">
					<li>So your preschool is up and running ?</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--- new preschool section end -->

<!--- existing preschool section start -->
<section>
	<div class="container">
		<div class="row">
			<div class="heading-text heading-section text-center">
				<h2 style="text-transform: uppercase;">Existing Preschool</h2>
				<div>Existing preschool owners have the opportunity to tap into our personal experience of having transformed a franchisee led business into a more meaningful and financially viable self-branded learning centre that actually works! If you’re looking for a way out with your current franchisor, not only do we have the right solution for you but also a carefully curated strategy to build your own brand and find acceptability within the parenting fraternity. This is a practical solution backed by years of research. The Zero to One Service Package helps existing preschools step out of the shadows of their franchisors.</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<iframe width="560" height="315" loading="lazy" src="https://www.youtube.com/embed/Ux8H4NIzwb4?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<h3>From Zero to One</h3>
				<span>From ideation to creation</span>
				<ul class="list-icon list-icon-check list-icon-colored">
					<li>Tired of being under the shadow of a franchisor ?</li>
					<li>Thinking of upgrading your existing preschool ?</li>
					<li>Upgrading is not an ON and OFF switch. Know how we PLAN it for you.</li>
				</ul>
			</div>
			<div class="col-lg-6">
				<iframe width="560" height="315" loading="lazy" src="https://www.youtube.com/embed/9PJLkB3f_70?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<h3>From One to Infinity</h3>
				<span>From creation to execution</span>
				<ul class="list-icon list-icon-check list-icon-colored">
					<li>When you know that you can do better but don’t know how ?</li>
					<li>When you need help implementing the curriculum and on various other aspects of preschooling.</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!--- existing preschool section end -->

<!--- high school section start -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="heading-text heading-section">
					<h2>High School</h2>
				</div>
			</div>
			<div class="col-lg-9">
				High Schools looking to set up their pre-primary sections benefit from our expertise in administering a world class preschool curriculum solution along with identification of all the equipment providers necessary to get the pre-primary section started.
			</div>
		</div>
	</div>
</section>
<!--- high school section end -->
