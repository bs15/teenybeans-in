<!DOCTYPE html>
<html lang="bn">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
<meta name="author" content="Teeny Beans" />
<link rel="icon" type="image/png" sizes="16x16" href="/images/icon/favicon-16x16.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge">


<title></title>
<meta name="description" content="">
<link rel="canonical" href="https://teenybeans.in/bn/" />

<link href="/css/plugins.css" rel="stylesheet" async>
<link href="/css/style.css" rel="stylesheet" async>

</head>
<body>
	<div class="body-inner">
		<!--- header section start -->
		<header id="header" data-transparent="true" class="dark header-logo-center">
		    <div class="header-inner">
		        <div class="container">
		            <div id="logo">
		                <a href="/">
		                    <span class="logo-default">
		                        <img src="/images/web/teenybeans-logo.png" alt="logo" style="max-width: 75%; vertical-align: top;">
		                    </span>
		                    <span class="logo-dark">
		                        <img src="/images/web/teenybeans-logo.png" alt="logo" style="max-width: 75%; vertical-align: top;">
		                    </span>
		                <!-- <span class="logo-dark">Teeny Beans</span> -->
		                </a>
		            </div>

		            <div id="mainMenu-trigger">
		                <a class="lines-button x"><span class="lines"></span></a>
		            </div>

		            <div id="mainMenu">
		                <div class="container">
		                    <nav>
		                        <ul>
		                            <li><a href="/">Home</a></li>
		                            <li><a href="/about">About</a></li>
		                            <li><a href="/curriculum">Curriculum</a></li>
		                            <li class="dropdown"><a href="/services">Services</a>
		                                <ul class="dropdown-menu">
		                                    <li class=""><a href="/support-system">Support System</a></li>
		                                </ul>
		                            </li>
		                            <li><a href="/faq">FAQ</a></li>
		                        </ul>

		                        <ul>
		                            <li><a href="/franchise-opportunity">Franchise</a></li>
		                            <li><a href="/preschool-at-home">Home Preschool</a></li>
		                            <li><a href="/blog">Blog</a></li>
		                            <li><a href="/contact">Contact</a></li>
		                        </ul>
		                    </nav>
		                </div>
		            </div>
		        </div>
		    </div>
		</header>
		<!--- header section end -->
		<!--- slider section start -->
		<div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
		    <div class="slide kenburns" id="slide0">
		        <div class="bg-overlay"></div>
		        <div class="container">
		            <div class="slide-captions text-center text-light">
		                <span class="strong">নিজস্ব ব্র্যান্ডযুক্ত প্যারেন্ট অ্যাপ্লিকেশনের মাধ্দ্ধম সম্পূর্ণ প্রি-স্কুল ডিজিটাল পাঠ্যক্রমটির মাধ্যমে নিজস্ব অনলাইন ক্লাস শুরু করুন - 100% বিনামূল্যে</span>
		                <h2 class="text-dark">হোম প্রি-স্কুল @ টিনি বিনস</h2>
		                <a class="btn btn-light" href="/preschool-at-home">Know More</a>
		            </div>
		        </div>
		    </div>

		    <div class="slide kenburns" id="slide1">
		        <div class="bg-overlay"></div>
		        <div class="container">
		            <div class="slide-captions text-center text-light">
		                <span class="strong">নিজস্ব প্রিস্কুল ব্র্যান্ডের মালিক হন ।  স্বল্পব্যয় সম্পন্ন এবং দীর্ঘমেয়াদী প্রাক বিদ্যালয়ের সমাধান। ১৩ মাসের মধ্যে বিনিয়োগ ফেরত পান !</span>
		                <h2 class="text-dark">
		                    জিরো রয়্যালটি | নন-ফ্র্যাঞ্চাইজ | অসীমিত সময়কাল
		                </h2>
		                <a class="btn btn-light" href="/contact">Contact Now</a>
		            </div>
		        </div>
		    </div>
		</div>
		<!--- slider section end -->

		<!--- about section start -->
		<section class="background-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-3">
		                <div class="heading-text heading-section">
		                    <!-- <h2>Non Franchise Preschool</h2> -->
		                    <h1 style="font-size: 48px;">নন ফ্র্যাঞ্চাইজ প্রিস্কুল</h1>
		                </div>
		            </div>
		            <div class="col-lg-9">
		                <div class="row">
		                    <div class="col-lg-6">
		                        <strong>টিনি বিনস</strong> শিশুদের শিক্ষার জায়গার ক্ষেত্রে নতুন এবং বিদ্যমান অনুশীলনকারীদের জন্য একটি &nbsp;নন-ফ্র্যাঞ্চাইজ &nbsp;এবং রয়্যালটি ছাড়া এককালীন সমাধান। আমরা আন্তর্জাতিক প্রাথমিক শিখন কেন্দ্র স্থাপন এবং পরিচালনা করতে সহায়তা করি যা বিশ্বব্যাপী শ্রেষ্ঠত্বের কেন্দ্রস্থল।
		                    </div>
		                    <div class="col-lg-6">
		                        এটি করার মাধ্যমে আমরা এমন প্রতিষ্ঠান তৈরি করি যা শৈশবকালীন শিক্ষা এবং যত্নের ক্ষেত্রে সর্বোচ্চ মান গ্রহণ করে এবং প্রাথমিক সম্প্রদায়ের শৈশব শিক্ষার ক্ষেত্রে স্থানীয় সম্প্রদায়ের কাছে পরিসেবা পৌঁছে দিচ্ছি।
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<!--- about section end -->

		<!--- curriculum section start -->
		<section>
			<div class="container">
				<div class="row">
					<div class="heading-text heading-section text-center">
						<h2>পাঠ্যক্রম</h2>
						<div>টিনি বিনসের আন্তর্জাতিক প্রাক বিদ্যালয়ের পাঠ্যক্রমটি বছরের পর বছর ধরে যত্ন সহকারে তৈরি করা হয়েছে এবং বিন্সটক (Beanstalk) আন্তর্জাতিক প্রাক বিদ্যালয়ে প্রয়োগ করা হয়েছে। আমাদের পাঠ্যক্রমের বিভিন্ন দিক আন্তর্জাতিক বৈধতার সুবিধা উপভোগ করে যেহেতু এটি  আন্তর্জাতিক পাঠ্যক্রম বিশেষজ্ঞ এবং প্রাথমিক শৈশবকালীন অনুশীলনকারীদের দ্বারা নির্ধারিত হয়েছে। আন্তর্জাতিক মন্টেসরি সোসাইটি (আইএমএস) এবং আন্তর্জাতিক মন্টেসরি কাউন্সিলের (আইএমসি) মতো স্বনামধন্য শৈশবকালীন ইনস্টিটিউটগুলির সাথে আমাদের সংযুক্তি আছে এবং আমরা আমাদের পাঠ্যক্রমকে আন্তর্জাতিক মানের শিক্ষা ব্যবস্থার সাথেও চিহ্নিত করতে সক্ষম হয়েছি।</div>
					</div>
				</div>
			</div>
		</section>

		<!--- eyfs intro section start -->
		<section class="background-grey">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-4">
		                <div class="heading-text heading-section">
		                    <h2>মূল পাঠ্যক্রম</h2>
		                </div>
		            </div>
		            <div class="col-lg-8">
		                আর্লি ইয়ার্স ফাউন্ডেশন স্টেজ (ইওয়াইএফএস - EYFS) কাঠামোটি গ্রহণের ক্ষেত্রে আমরা ব্রিটিশ পাঠ্যক্রম অনুসরণ করি। ইওয়াইএফএস (EYFS) কাঠামোটি শিশুদের সৃজনশীল ভাব, শারীরিক এবং যোগাযোগ দক্ষতার পাশাপাশি সংবেদনশীল এবং সামাজিক বিকাশের প্রতি সংহত মনোভাব গড়ে তোলে।  পড়াশোনার কাঠামোটি যত্ন সহকারে তৈরি করা হয়েছে  অনুসন্ধান, শিশু-দীক্ষিত অভিজ্ঞতার কথা মাথায় রেখে।
		            </div>
		        </div>
		    </div>
		    <div class="container">
		        <div class="heading-text heading-line text-center">
		            <h4>ইওয়াইএফএস (EYFS) নীতি </h4>
		        </div>
		        <div class="row team-members team-members-shadow">
		            <div class="col-lg-3">
		                <div class="team-member">
		                    <div class="team-image">
		                        <img src="/images/web/eyfs-unique-child.jpg" alt="A Unique Child" />
		                    </div>
		                    <div class="team-desc">
		                        <h3>একটি অনন্য শিশু</h3>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3">
		                <div class="team-member">
		                    <div class="team-image">
		                        <img src="/images/web/eyfs-positive-relationship.jpg" alt="A Positive Relationship" />
		                    </div>
		                    <div class="team-desc">
		                        <h3>একটি ইতিবাচক সম্পর্ক</h3>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3">
		                <div class="team-member">
		                    <div class="team-image">
		                        <img src="/images/web/eyfs-enabling-environment.jpg" alt="Enabling Environment" />
		                    </div>
		                    <div class="team-desc">
		                        <h3>পরিবেশ সক্ষম করা</h3>
		                    </div>
		                </div>
		            </div>
		            <div class="col-lg-3">
		                <div class="team-member">
		                    <div class="team-image">
		                        <img src="/images/web/eyfs-learning-development.jpg" alt="Learning and Development" />
		                    </div>
		                    <div class="team-desc">
		                        <h3>শেখা | উন্নয়ন</h3>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<!--- eyfs intro section end -->

		<!--- augmented curriculum section start -->
		<section>
			<div class="container">
				<div class="row">
					<div class="heading-text heading-section text-center">
						<h2>সংযুক্ত পাঠ্যক্রম</h2>
						<div>টিনি বিনসের সংযুক্ত পাঠ্যক্রমটি খুব যত্ন সহকারে বেশ কয়েকটি কাঠামো সংযোজন করেছে যেগুলি পিতামাতার দৃষ্টিভঙ্গির ওপর নির্ভরশীল। যদিও আমাদের মূল পাঠ্যক্রমটি শৈশবকালীন শিক্ষার ক্ষেত্রে বিকাশের প্রয়োজনীয় প্রতিটি অংশে যত্নশীল, এবং বিষয়গুলি আরও গভীর, যা পিতামাতার দৃষ্টিকোণ থেকে গুরুত্বপূর্ণ হিসাবে বিবেচিত। পিতামাতার দৃষ্টিভঙ্গিগুলি ভিন্ন ভৌগলিক এবং সংস্কৃতি সত্তেও অত্যন্ত প্রাসঙ্গিক। টিবিপিসিতে (TBPC) নলেজ একাডেমির বহু বছরের গবেষণার মাধ্যমে সংযুক্ত পাঠ্যক্রমের অংশগুলি যত্নসহকারে সংশোধন করা হয়েছে, বয়স হিসেবে ভাগ করা হয়েছে এবং আধুনিক শিক্ষায় অগ্রণী ভূমিকা রাখছে। প্রচলিত প্রাক বিদ্যালয়ের পাঠ্যক্রমের চেয়ে অনেক এগিয়ে আমাদের এই পাঠ্যক্রম।</div>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="content col-lg-9">
						<div class="carousel arrows-visibile testimonial testimonial-single" data-items="1" data-loop="true" data-autoplay="true" data-autoplay="3500" data-arrows="true" data-animate-in="fadeIn" data-animate-out="fadeOut">
							<div class="testimonial-item">
								<img src="/images/web/writo.jpg" alt="Writo - A handwriting program for kids">
								<h3>হাতের লেখা</h3>
								<span>Writo</span>
								<span>প্রি-স্কুল শিশু এবং কিশোরদের জন্য একটি  বোধশক্তিসম্পন্ন হস্তাক্ষর কর্মসূচি। 'দেখুন এবং শিখুন' পদ্ধতির মাধ্যমে প্রস্তাবিত ঐতিহ্যবাহী ভিজ্যুয়াল কপির কৌশলটির বিপরীতে, রাইটো (Writo) বৈজ্ঞানিকভাবে প্রগতিশীল পরিচালিত বা আন্দোলন ভিত্তিক শিক্ষাকে গ্রহণ করে যা স্থায়ী স্নায়বিক পথ তৈরিতে একাধিক জ্ঞানের অঙ্গকে প্রসারিত করে।</span>
							</div>
							<div class="testimonial-item">
								<img src="/images/web/minimax.jpg" alt="Minimax - brain developmental program for children">
								<h3>মস্তিষ্কের বিকাশ </h3>
								<span>Mini Max</span>
								<span>একটি সম্পূর্ণ মস্তিষ্কের বিকাশমূলক প্রোগ্রাম যা শিশুর শেখার খুব গোড়ার দিকে পুরো মস্তিষ্কের বিকাশের লক্ষ্য। প্রাক বিদ্যালয়ের বয়স থেকেই ডান মস্তিষ্কের সক্রিয়করণ, আমাদের মস্তিষ্কের সম্পূর্ণ সম্ভাব্যতা অর্জনে সহায়তা করে। মিনিম্যাক্স অ্যাবাকাস (Minimax Abacus)  এটা করতে সহায়তা করে।</span>
							</div>
							<div class="testimonial-item">
								<img src="/images/web/super-phonics.jpg" alt="Super Phonics - English Communication program for children">
								<h3>ইংরাজী শিক্ষা</h3>
								<span>Super Phonics</span>
								<span>প্রাক বিদ্যালয়ের জন্য একটি সম্পূর্ণ ইংরাজী পাঠ্যক্রম, পড়া এবং লেখার জন্য একটি শক্তিশালী ভিত্তি বিকাশ করে। এটি চিঠির শব্দগুলি শেখানোর সমন্বয়ী পাঠ্যক্রম পদ্ধতিটি এমনভাবে উপস্থাপিত করেছে যা মজাদার এবং বহু সংবেদনশীল। শিশুরা শব্দটি পড়তে এবং লিখতে অক্ষরটি কীভাবে ব্যবহার করতে হয় তা শিখতে পারে।</span>
							</div>
							<div class="testimonial-item">
								<img src="/images/web/bonjour.jpg" alt="Bonjour - Accelerated Cognitive Development">
								<h3>তাত্ক্ষণিক জ্ঞানীয় বিকাশ</h3>
								<span>Bonjour</span>
								<span>শিশুরা প্রাকৃতিক ভাষা পুনরাবৃত্তি এবং খেলার বিশ্লেষণের মাধ্যমে সহজেই শিখে যায়।  প্রমাণগুলি দেখায় যে বিদেশী ভাষাগুলির প্রথম দিকের প্রকাশ স্থানীয় উচ্চারণ বিকাশ করতে সহায়তা করে, সমস্ত ক্ষেত্রে পরীক্ষার স্কোর বাড়ায়, বাচ্চাদের একটি বিশ্বব্যাপী দৃষ্টিভঙ্গি এবং সাংস্কৃতিক বোঝার বিকাশ করতে সহায়তা করে এবং অধ্যয়নের দীর্ঘতর ধারাবাহিকতা এবং ভাষা দক্ষতার উচ্চ স্তরের জন্য অনুমতি দেয়।</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--- augmented curriculum section end -->

		<!--- about section start -->
		<section style="background-image:url('/images/web/general-bg.jpg');">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="heading-text heading-section">
							<h2 style="color: #fff;">সমন্বয় শিক্ষা কেন্দ্র - ইন্টিগ্রেটেড লার্নিং সেন্টার (আইএলসি)</h2>
							<div style="color: #fff;"> আইএলসি (ILC) হ'ল এমন একটি প্রতিষ্ঠান যা শৈশবকালীন শিক্ষায় দক্ষতা বহন করে জন্ম থেকে শৈশব  পর্যন্ত। এটি ৬ মাস – ৯ বছর বয়সের বাচ্চাদের জন্য বয়স-উপযুক্ত প্রোগ্রামগুলি সঠিকভাবে সনাক্ত এবং পরিচালনা করে।<br><br>
							আইএলসি-র মালিকদের জন্য এটি একাধিক উপার্জনের সুবিধা সম্পন্ন বিজনেস মডেলে বিনিয়োগ করার পাশাপাশি, অত্যন্ত সম্মানজনক প্রাথমিক শিক্ষামূলক কেন্দ্র পরিচালনায় অংশ নেওয়া,  যা আইএলসিকে গ্রহণযোগ্য প্রস্তাব হিসেবে বাকিদের থেকে পৃথক করে।</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/5ei92WT1BHU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--- about section end -->

		<!--- card section start -->
		<section class="background-grey">
			<div class="container">
				<div class="row team-members team-members-shadow">
					<div class="col-lg-4">
						<div class="team-member">
							<div class="team-image">
							<img src="/images/web/preschool.jpg" alt="International Preschool">
							</div>
							<div class="team-desc">
								<h3>একটি আন্তর্জাতিক প্রাক বিদ্যালয়</h3>
								<p>যুক্তরাজ্যের ইওয়াইএফএস (EYFS) পাঠ্যক্রমের গাইডলাইন অনুসরণ করে একটি আন্তর্জাতিকভাবে স্বীকৃত পাঠ্যক্রম অনুসরণ করা হয়। এই স্কুল শিশুর গঠনমূলক বছরগুলিতে বেশ কয়েকটি সংযুক্ত প্রোগ্রামের সাথে প্রাথমিক শিক্ষার জন্য ভিত্তি প্রস্তর স্থাপন করে।</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="team-member">
							<div class="team-image">
							<img src="/images/web/teacher-training.jpg" alt="International Teacher Training Centre">
							</div>
							<div class="team-desc">
								<h3>আন্তর্জাতিক শিক্ষক প্রশিক্ষণ কেন্দ্র</h3>
								<p>আন্তর্জাতিক মন্টেসরি শিক্ষক প্রশিক্ষণ কেন্দ্র যেটি আন্তর্জাতিক মানের শংসাপত্র  প্রদান করে।</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="team-member">
							<div class="team-image">
							<img src="/images/web/afterschool.jpg" alt="After School Activity Centre">
							</div>
							<div class="team-desc">
								<h3>কার্যাশক্তি কেন্দ্র</h3>
								<p>বিন্সটক 3 পি (Beanstalk 3P), রাইটো, ম্যাক্সব্রেন অ্যাবাকাস, সুপার ফোনিক্স এবং কেমব্রিজ ইয়াং লার্নার্স ইংলিশ- সমস্ত বয়সের জন্য প্রোগ্রাম এবং নির্বিঘ্নে প্রাক বিদ্যালয়ের প্রোগ্রামের সাথে একীভূত।</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!--- card section end -->

		<!--- about section start -->
		<section>
			<div class="container">
				<div class="row">
					<div class="heading-text heading-section">
						<h2>কেন টিনি বিনস আন্তর্জাতিক মানের প্রি-স্কুল ?</h2>
					</div>
					<div class="col-lg-9">
							<div>
								<p>এমন অনেকগুলি কারণ রয়েছে যা একটি ইন্টিগ্রেটেড লার্নিং সেন্টারকে (ILC) আন্তর্জাতিক মানের প্রাথমিক শিক্ষার কেন্দ্র হিসাবে উপস্থাপিত করে ।</p>
								<ul style="list-style-type: square;">
									<li>(USA) যুক্তরাষ্ট্রের আন্তর্জাতিক মন্টেসরি কাউন্সিল (আইএমসি) পাশাপাশি আন্তর্জাতিক মন্টেসরি সোসাইটি (আইএমএস), মার্কিন যুক্তরাষ্ট্রের স্বীকৃতি প্রাপ্ত একমাত্র প্রাক বিদ্যালয় পাঠ্যক্রম।</li>
									<li>শৈশবকালীন শিক্ষার জন্য মূল পাঠ্যক্রম হিসাবে একটি ব্রিটিশ ফ্রেমওয়ার্ক (ইওয়াইএফএস) গ্রহণ করে।</li>
									<li>আন্তর্জাতিক খ্যাতিযুক্ত পাঠ্যক্রম অনুসরণ করে।</li>
									<li>বিশ্বের কাছে ২০৫০ এবং তার পরবর্তী সময়ের কথা ভেবেই এই পাঠ্যক্রম বানানো।</li>
									<li>প্রি-স্কুল স্তরে ফরাসি (French)  পড়ানোর একমাত্র পাঠ্যক্রম।</li>
								</ul>
								<p>শৈশবকালীন অনুশীলনকারীদের জন্য আমরা মনোযোগ সহকারে একটি দুর্দান্ত পাঠ্যক্রম রেখেছি। আমরা আমাদের সমস্ত অংশীদারদের, আমাদের বিদ্যালয়ের শিক্ষিকাগণ এবং আমাদের প্রিয় পিতামাতাদের শুভেচ্ছা জানাই!</p>
							</div>
					</div>
					<div class="col-lg-3 img-responsive">
						<img src="/images/web/teenybeans-international-preschool-curriculum.jpg" alt="Teeny Beans International Curriculum logo" class="img-responsive">
					</div>
				</div>
			</div>
		</section>
		<!--- about section end -->

		<!--- faq section start -->
		<section id="faq" class="background-grey">
			<div class="container">
				<div class="heading-text heading-section">
					<h2>প্রায়শই জিজ্ঞাসিত প্রশ্নাবলী</h2>
				</div>
				
				<div class="accordion accordion-shadow">
				    <div class="ac-item">
				        <h5 class="ac-title">1. টিনি বিনসের প্রস্তাবিত সমাধান কী?</h5>
				        <div class="ac-content">
				            <p>আমরা স্বতন্ত্র ব্র্যান্ডযুক্ত প্রি-স্কুল এবং শৈশবকালীন শ্রেষ্ঠ শিক্ষার প্রতিষ্ঠান তৈরিতে সহায়তা করি।</p>
							<p>অন্যদের থেকে যেটা আমাদের আলাদা করে তোলে তা হ'ল আমরা একটি বৈচিত্র্যময় ব্যবসা প্রতিষ্ঠা করতে এবং পরিচালনা করতে সাহায্য করি এবং&nbsp; এটিকে &nbsp;আমরা সমন্বিতশিক্ষণ কেন্দ্র (<strong>ILC</strong>) বলে থাকি।</p>
							<ul class="list-icon list-icon-check">
							    <li>আমাদের সমন্বিতশিক্ষণ কেন্দ্রে যেগুলি থাকে -</li>
							    <li>একটি আন্তর্জাতিক প্রাক বিদ্যালয়</li>
							    <li>একটি আন্তর্জাতিক শিক্ষক প্রশিক্ষণ কেন্দ্র</li>
							    <li>একটি বাচ্চাদের মস্তিস্ক বিকাশ কেন্দ্র</li>
							</ul>
							<p>আমাদের সমাধানটি অংশীদারদের জন্য একটি সামগ্রিক ব্যবসায়িক সমাধান এবং সর্বাগ্রে আমাদের সমাধানটি হ'ল নন-ফ্র্যাঞ্চাইজি এবং &nbsp;রয়্যালটিহীন মডেল যেটা আমরা বিশ্বাস করি যে প্রাক-বিদ্যালয়ের শিক্ষণকেন্দ্রগুলির ক্ষেত্রে শ্রেষ্ঠ।</p>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">2. টিনি বিনস কীভাবে একটি প্রাক বিদ্যালয়ের ফ্র্যাঞ্চাইজি থেকে আলাদা?</h5>
				        <div class="ac-content">
				            <p>আমরা কেবলমাত্র পাঠ্যক্রম, বিতরণ, প্রশিক্ষণ, নিয়োগ, পিতামাতার চিন্তা ইত্যাদির বিষয়ে আগ্রহী, যেগুলি প্রাক-বিদ্যালয়ের কোনও ফ্র্যাঞ্চসাইজারের জন্য সর্বশেষ তাত্পর্য বহন করে। এটি বিদ্যমান প্রাক-বিদ্যালয়ের ফ্র্যাঞ্চাইজি মালিকদের সাথে দৃঢ়ভাবে সম্পর্কিত।</p>
				            <p>আমরা রয়্যালটি এবং কমিশন ছাড়া এককালীন বিনিয়োগ অফার করি। আমরা আপনার সাথে পাঠ্যক্রমটি সহ-বাস্তবায়ন করি এবং চিরকাল আপনার সাথে আইএলসি সহ-পরিচালনা করি। আমাদের লক্ষ্য হবে  বিপণন এবং একটি চমৎকার পাঠ্যক্রমের প্রয়োগের মাধ্যমে আপনার ব্র্যান্ডকে আপনার লোকালয়ের মধ্যে জনপ্রিয় করে তোলা।</p>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">3. আমি একটি প্রি-স্কুল শুরু করতে চাইলে আপনি আমাকে কী কী পরিষেবা দেবেন?</h5>
				        <div class="ac-content">
				            <p>আপনি যখন একিসাথে একটি প্রি-স্কুল, একটি শিক্ষক প্রশিক্ষণ কেন্দ্র এবং একটি শিশু সক্রিয়তা কেন্দ্র খুলতে পারেন তখন কেন কেবল শুধুমাত্র একটি প্রি-স্কুল খুলবেন?</p>
				            <p>আমাদের পরিষেবাগুলি আপনাকে কেবলমাত্র একটি সমন্বিত শিক্ষামূলক কেন্দ্র শুরু করতে নয়, এটি সফলভাবে চালাতেও সহায়তা করে। আমাদের পরিষেবাগুলির আরও বিশদ বিবৃতি রয়েছে, এখানে সংক্ষিপ্তসার হিসাবে দেওয়া হল -</p>
				            <p><strong>ক। প্রাথমিক&nbsp; পরিষেবা:</strong></p>
							<p>ব্যবসা বোঝার উপর প্রশিক্ষণ প্রোগ্রাম, আর্থিক পরিকল্পনা এবং ওভারহেড নিয়ন্ত্রণ উপর প্রশিক্ষণ এবং আইএলসি মালিকের পাশাপাশি সমস্ত শিক্ষণ কর্মীদের জন্য একটি গভীরতর পাঠ্যক্রম প্রশিক্ষণ।</p>
							<p><strong>খ। স্কুল সংক্রান্ত পরিসেবাঃ</strong></p>
							<p>স্থান নির্বাচন, স্কুল গঠনপ্রণালী, শিক্ষার উপকরণ , স্বতন্ত্রভাবে প্রি-স্কুলের নাম প্রচার করা (অফলাইন এবং ডিজিটাল উভয়) এবং নিয়োগের জন্য সহায়তা করা।</p>
							<p><strong>গ। বিদ্যালয়ের কারজপ্রনালিঃ</strong></p>
							<p>পরিচালনায় সহায়তা, পাঠ্যক্রম বাস্তবায়ন, প্রযুক্তিগত সহায়তা, এবং শিশুদের ভর্তির মাধ্যমে বিপণন সমর্থন, পোস্ট অ্যাডমিশন</p>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">2. টিনি বিনস কীভাবে  প্রশিক্ষণ দেয়?</h5>
				        <div class="ac-content">
				            <p>প্রশিক্ষণ একটি অবিচ্ছিন্ন প্রক্রিয়া। এই কারণেই আমরা এই জায়গাতে একমাত্র সমাধান প্রদানকারী হব যা বিক্রয়-পরবর্তী সম্পর্ক ছিন্ন করে না তবে বাস্তবে আপনার স্কুলটি চালু হয়ে গেলে ব্যস্ততা বাড়িয়ে তোলে। প্রশিক্ষণের ধাপগুলি নিম্নরূপ -</p>
							<ul class="list-icon list-icon-check">
							    <li>আমরা কলকাতায় আমাদের মডেল প্রিস্কুল বিনস্টক ইন্টারন্যাশনাল প্রিস্কুলে একটি 3 দিনের ট্রেনিং দিয়ে থাকি।</li>
							    <li>প্রিস্কুলের মালিকরা ব্যবসায়ের বেসিকগুলি এবং একটি সফল আইএলসি প্রতিষ্ঠায় তার নিজের পেশাদার যাত্রা বুঝতে আমাদের সহ-প্রতিষ্ঠাতার সাথে একটি স্কাইপ (Skype) সেশনও রাখবেন।</li>
							    <li>আমাদের প্রি-স্কুল সেটআপ এবং কারিকুলাম প্রশিক্ষণের বিভিন্ন দিকগুলির একটি ই-লার্নিং মডিউলও রয়েছে যা নিজের বোধশক্তি বাড়িয়ে তোলে।</li>
							    <li>প্রিস্কুলে যোগদানকারী প্রতিটি নতুন শিক্ষিকা পাঠ্যক্রম প্রশিক্ষণের জন্য একই ই-লার্নিং মডিউলটি ব্যবহার করতে পারবেন।</li>
							</ul>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">2. আপনি কি আপনার পরিষেবার জন্য রয়্যালটি নিয়ে থাকেন?</h5>
				        <div class="ac-content">
				            <p>আমরা একটি নন-ফ্র্যাঞ্চাইজি, জিরো রয়্যালটি মডেল পরিচালনা করি। আমরা আপনাকে চিরস্থায়ী স্বতন্ত্র ব্র্যান্ডযুক্ত আইএলসি তৈরি করতে সহায়তা করি এবং আপনাকে  প্রাথমিক শিক্ষার একজন বিশেষজ্ঞ হয়ে উঠতে যথেষ্ট সহায়তা করব।</p>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">2. আপনি কি প্রি-স্কুলের কোন শিক্ষার সরঞ্জাম প্রস্তুত করেন?</h5>
				        <div class="ac-content">
				            <p>না আমরা করি না, তবে এব্যাপার আমরা &nbsp;দুটি উপায় দিতে পারি -</p>
							<ul class="list-icon list-icon-check">
							    <li>সেরা সম্ভাব্য মূল্যে সেরা মানের প্রস্তুতকারকের ঠিকানা। আমরা কয়েক বছর ধরে এটি করেছি, এই সুবিধাটি আপনার কাছে পৌঁছে দেব।</li>
							    <li>আমরা মনোনিবেশ করেছি সর্বোত্তমভাবে অসামান্য শিক্ষাগত পণ্য তৈরি করা এবং অসামান্য সেটা গ্রাহকদের কাছে পৌঁছে দেওয়া।</li>
							</ul>
							<p>
							    আপনাকে প্রস্তাবিত এবং সরবরাহিত প্রতিটি শিক্ষার সহায়তা এবং সরঞ্জামগুলি পাঠ্যক্রমের একটি অনন্য দিকটিতে ঠিক ম্যাপ করা হয়েছে। অপ্রয়োজনীয় আইটেমগুলি ক্রমবর্ধমানভাবে আনার জন্য আমাদের কোনও ইচ্ছা বা প্রেরণা নেই যা কেবলমাত্র আমরা সেগুলি
							    উত্পাদন করতে পারি বলেই ব্যবহৃত হয় না!
							</p>
							<p>আমরা কোনও নির্দিষ্ট বিক্রেতার সাথে ব্যবসা করিনা এবং তাই আপনি সেরা বিকল্প থেকে সরঞ্জাম কিনতে পারেন, এইব্যপারে আমরা সম্পূর্ণ দায়িত্ব আপনাদের ওপরই রাখি।</p>
				        </div>
				    </div>
				    <div class="ac-item">
				        <h5 class="ac-title">2. আমার কি প্রতি বছর পাঠ্যক্রমের জন্য নতুন অর্থ প্রদান করতে হবে?</h5>
				        <div class="ac-content">
				            <p>বিন্সটকের নলেজ একাডেমি, যা টিনি বিন্সের মূল সংস্থা, ইতিমধ্যে একটি দুর্দান্ত পাঠ্যক্রমের পর্যায়ক্রমিক সংশোধন করে। আইএলসি এই জাতীয় সংশোধনগুলির জন্য অর্থ প্রদান করে না এবং আরও কার্যকরভাবে পরিচালনা করার জন্য প্রশিক্ষিত হয়। যদি বিন্সটকের পাঠক্রম বাণিজ্যিক ব্যবহারের জন্য নতুন শিক্ষামূলক প্রোগ্রাম চালু করে থাকে তবে কোনও আইএলসি তাদের ইনস্টিটিউটে এটি প্রয়োগের আগে এটি মূল্যায়ন করতে পারে। তবে এমন করার মতো কোনও বাধ্যবাধকতা নেই।</p>
				        </div>
				    </div>
				</div>
			</div>
		</section>
		
		<!--- faq section end -->

		<!--- testimonials section start -->
		<section style="background-image:url('/images/web/general-bg.jpg');">
		  <div class="container">
		    <div class="heading-text text-center text-light">
		      <h2>প্রশংসাপত্র</h2>
		    </div>

		    <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left text-light" data-items="1">
		      <div class="testimonial-item">
		        <img src="/images/web/stepping-stones-ip-owner.jpeg" alt="Stepping Stones IP Owner">
		        <p>আমি টিনি বিন্সের জন্য অত্যন্ত কৃতজ্ঞ, কেননা যখন আমি নাগপুরে একটি প্রাক বিদ্যালয় খোলার পরিকল্পনা করছিলাম তখন আমি কীভাবে শুরু করব এবং কী করব এবং জিনিসগুলি খুব অগোছালো হয়ে যাচ্ছিল সে সম্পর্কে আমি খুব বিভ্রান্ত হয়ে পড়েছিলাম তখন আমি একজন সঙ্গীর সন্ধান করতে শুরু করি, তখন আমি টিনি বিন্সের কথা জানতে পারি। সংস্থার  কোনও রয়্যালটি, কোনও আমানত জমা ছাড়াই একটি সম্পূর্ণ প্রি-স্কুল পেয়ে আমি খুব খুশি।</p>
		        <span>Sudha Mangesh Gosavi </span>
		        <span>Director of Steeping Stones International Preschool</span>
		      </div>

		      <div class="testimonial-item">
		        <img src="/images/web/nest-ip-owner.jpeg" alt="Nest IP Owner">
		        <p>এই ব্যবসায় একজন শিক্ষানবিস হয়ে, আমি প্রি-স্কুল খোলার জন্য কিছুটা টেনশনে ছিলাম ... তবে পুরো প্রশিক্ষণ প্রক্রিয়া, সুবিধাগুলি, অঞ্চল নির্বাচন, প্রাক বিজ্ঞাপন, ইত্যাদি সম্পর্কিত অবিচ্ছিন্ন সহায়তা এবং সমর্থন আমাকে আত্মবিশ্বাসী এবং সন্তুষ্ট করে তুলেছে।</p>
		        <span>Sharmistha Misra</span>
		        <span>Director of NEST International Preschool </span>
		      </div>

		    </div>

		  </div>
		</section>
		<!--- testimonials section end -->

		<!--- 6th section start -->
		<section>
			<div class="container">
				<div class="row">
					<div class="heading-text heading-section">
						<h2 style="text-transform: uppercase;">টিনি বিনসের প্লেস্কুলের সুবিধাসমূহ -</h2>
					</div>
					<div class="col-lg-12">
						<ul class="list-icon list-icon-check list-icon-colored">
							<li>কোনও ফ্র্যাঞ্চাইজ ফি নেই।</li>
							<li>কোন রয়্যালটি ফি নেই।</li>
							<li>একটি আন্তর্জাতিক খ্যাতিযুক্ত স্কুল স্কুল চেইন।</li>
							<li>ব্রিটিশ পাঠ্যক্রম, আন্তর্জাতিক প্রাক বিদ্যালয়ে প্রয়োগ করা হয়।</li>
							<li>প্রাক বিদ্যালয়ের ইনপুট: পাঠ্যক্রম, প্রসপেক্টাস, শিক্ষামূলক উপকরণ, বই ইত্যাদি;</li>
							<li>কম বিনিয়োগের সাথে প্লেস্কুল ফ্র্যাঞ্চাইজি।</li>
							<li>সাশ্রয়ী মূল্যে আন্তর্জাতিক শিক্ষা </li>
							<li>আপনার স্কুল এবং যোগাযোগের তথ্য প্রদর্শনের জন্য একটি ওয়েবপেজ (webpage)</li>
							<li>আপনার ব্র্যান্ড তৈরি করতে ডিজিটাল বিপণন।</li>
							<li>অংশীদার কেন্দ্র পরিচালনার জন্য প্রযুক্তিগত সহায়তা</li>
							<li>পাঠ্যক্রমের বিতরণের জন্য প্যারেন্ট অ্যাপস এবং স্কুল পরিচালনার জন্য প্রযুক্তিগত সহায়তা।</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--- 6th section end -->

		<!--- footer section start -->
		<footer id="footer" class="inverted">
		    <div class="footer-content">
		      <div class="container">
		        <div class="row">

		          <div class="col-xl-4 col-lg-4 col-md-4">
		            <p><a href="#"><img src="/images/web/tb-logo-flat.png" alt="teenybeans logo"></a></p>
		            <p>টিনি বিনস শৈশবকালীন শিক্ষার জায়গার ক্ষেত্রে নতুন এবং বিদ্যমান অনুশীলনকারীদের জন্য একটি নন ফ্র্যাঞ্চাইজ এবং রয়্যালটি ছাড়া সামগ্রিক সমাধান।</p>
		            <h4><strong>ডাউনলোড করুন SPARSH</strong></h4>
		            <a id="gplaybutton" href='https://play.google.com/store/apps/details?id=com.beanstalkedu.app.sparsh&utm_source=website&utm_campaign=tb_web&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img class="img-responsive" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>
		          </div>

		          <div class="col-xl-4 col-lg-4 col-md-4">
		            <div class="widget  widget-contact-us" style="background-image: url('/images/web/world-map-dark.png'); background-position: 50% 20px; background-repeat: no-repeat">
		            <h4>আমাদের সাথে যোগাযোগ করুন</h4>
		              <ul class="list-icon">
		                <li><i class="fa fa-map-marker-alt"></i> DE 2B, VIP Rd, Desh Bandhu Nagar,Baguiati<br>
		                Rajarhat, West Bengal 700059</li>
		                <li><i class="fa fa-phone"></i> +91 97483 86431</li>
		                <li><i class="far fa-envelope"></i> <a href="#">enquiry@beanstalkedu.com</a> </li>
		                <li> <br>
		                <i class="far fa-clock"></i>Monday - Saturday: <strong>09:00 - 18:00</strong> <br>
		                Sunday: <strong>Closed</strong> </li>
		              </ul>
		            </div>
		          </div>
		          <a href="/DOCS/Preschool-Business-Proposal.pdf" id="dLink" download style="display:none;"><i class="fa fa-envelope"></i> Download Corporate Brochure</a>

		          <div class="col-xl-4 col-lg-4 col-md-4">

		            <div class="widget clearfix widget-newsletter">
		              <h4 class="widget-title"></i> আমাদের নতুন অংশীদারি</a></h4>
		              <ul class="list-icon partner-sites">
		                <li><i class="fa fa-check"></i> <a href="/partners/nest-berhampore-westbengal/">Nest International Preschool</a></li>
		                <li><i class="fa fa-check"></i> <a href="/partners/steppingstones-nagpur-maharashtra/">Stepping Stones International Preschool</a></li>
		                <li><i class="fa fa-check"></i> <a href="/partners/bizzybees-vadodara-gujarat/">Bizzy Bees International Preschool</a></li>
		              </ul>
		              <h4 class="widget-title"></i>কর্পোরেট ব্রোশিওর ডাউনলোড করুন</a></h4>
		              <p>আমাদের অফারগুলি দেখুন।</p>
		              <p id="thankMsg" style="display: none;">Thank you! Please check your email for the brochure.</p>
		              <form class="widget-subscribe-form p-r-40" action="#" role="form" method="post" novalidate="novalidate">
		                <div class="input-group">
		                  <input aria-required="true" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" type="email" id="email2">
		                  <span class="input-group-btn">
		                  <button  id="widget-subscribe-submit-button dBtn" onclick="downloadBr();" class="btn"><i class="fa fa-paper-plane"></i></button>
		                   </span>
		                </div>
		              </form>
		              <p id="emailError2" style="display: none; color: red;" >Please Provide a valid email</p>
		            </div>
		          </div>


		        </div>
		      </div>
		    </div>
		    <div class="copyright-content">
		      <div class="container">
		        <div class="row">
		          <div class="col-lg-6">

		            <div class="social-icons social-icons-colored">
		              <ul>
		                <li class="social-facebook"><a href="https://www.facebook.com/teenybeanspreschoolcurriculum/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
		                <li class="social-youtube"><a href="https://www.youtube.com/channel/UC4ppJKZgGaOxexol-zMwZDQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
		                <li class="social-twitter"><a href="https://twitter.com/teeny_beans" target="_blank"><i class="fab fa-twitter"></i></a></li>
		                <li class="social-instagram"><a href="https://www.instagram.com/teenybeans.in/" target="_blank"><i class="fab fa-instagram"></i></a></li>
		              </ul>
		            </div>

		          </div>
		          <div class="col-lg-6 text-right">
		            <div class="copyright-text">
		              <a href="/terms-and-conditions">Terms & Conditions</a> | 
		            © 2020 টিনি বিনস  | সমস্ত অধিকার সংরক্ষিত. </div>
		          </div>
		        </div>
		      </div>
		    </div>
		</footer>
		<!--- footer section end -->
	</div>

	<a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
	<script type="text/javascript">
	  function downloadBr() {
	    var email = document.getElementById("email2").value;
	    var emailError2 = document.getElementById("emailError2");
	    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	    if (email == '') {
	      emailError2.style.display = "block";
	    }else{
	      if (reg.test(email) == false) 
	      { 
	          emailError2.style.display = "block";
	      }else{
	        emailError2.style.display = "none";
	        document.getElementById("email2").value = '';
	        document.getElementById("thankMsg").style.display = "block";
	        saveEmail(email);
	        sendEmail(email);
	      }
	    }
	    
	    // document.getElementById("thankMsg").style.display = "block";
	    

	  }
	  var message = '<html><body>Welcome to Teenybeans. You have showed interest to know more about us and here it is. You will find our Brochure Link in this mail. Download the brochure and know everything about us. <br>And for more information you can visit our website <a link="https://teenybeans.in">here</a>.<br><a href="https://teenybeans.in/DOCS/Preschool-Business-Proposal.pdf" download>Download Your Brochure Now!</a><br>-Team Teenybeans</body></html>';
	  function sendEmail(email) {
	    let formData = new FormData();
	      formData.append('sendMail', 'true');
	      formData.append('reciever', email);
	      formData.append('sender', 'Teenybeans');
	      formData.append('senderMail', 'no-reply@teenybeans.in');
	      formData.append('subject', "Teenybeans Brochure");
	      formData.append('message', message);
	      fetch("https://mailapi.teenybeans.in/", {
	            method: "POST",
	            body:formData,
	        }).then(
	            function(response) {
	            response.json().then(function(data) {
	              console.log(data);
	            });
	          }
	        )
	        .catch(function(err) {
	          console.log('Fetch Error :-S', err);
	        });
	  }
	  function saveEmail(email) {
	    let formData = new FormData();
	    formData.append('formName', 'teenyBeansBrochure');
	    formData.append('Email', email);
	    // formData.append('Address', addr);
	    fetch('https://api.teenybeans.in/API/contactFormProcessor/v1/', {
	      method: 'POST',
	      body: formData
	    })
	    .then(res => res.json())
	    .then(json =>  {
	      // window.location.href = "contact-back";
	      }
	    );
	  }
	</script>

	<script src="/js/jquery.js"></script>
	<script src="/js/plugins.js"></script>
	<script src="/js/functions.js"></script>

	<script type="text/javascript">
	  ( function() {

	  var youtube = document.querySelectorAll( ".youtube" );
	  
	  for (var i = 0; i < youtube.length; i++) {
	    
	    var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
	    
	    var image = new Image();
	        image.src = source;
	        image.addEventListener( "load", function() {
	          youtube[ i ].appendChild( image );
	        }( i ) );
	    
	        youtube[i].addEventListener( "click", function() {

	          var iframe = document.createElement( "iframe" );

	              iframe.setAttribute( "frameborder", "0" );
	              iframe.setAttribute( "allowfullscreen", "" );
	              iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

	              this.innerHTML = "";
	              this.appendChild( iframe );
	        } );  
	  };
	  
	} )();
	</script>


	</body>
</html>